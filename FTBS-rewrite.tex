%\documentclass[aps,pre,showkeys,showpacs,groupedaddress,amsmath,amssymb,floatfix,twocolumn]{revtex4-1}
\documentclass[openacc]{rstransa}%%%%where rstrans is the template name

%%%% *** Do not adjust lengths that control margins, column widths, etc. ***

%%%%%%%%%%% Defining Enunciations  %%%%%%%%%%%
\newtheorem{theorem}{\bf Theorem}[section]
\newtheorem{condition}{\bf Condition}[section]
\newtheorem{corollary}{\bf Corollary}[section]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage[utf8]{inputenc}
\usepackage{subcaption}

\usepackage{changes}
\definechangesauthor[name={Frank}, color=red]{FH}
\definechangesauthor[name={Paul}, color=green]{PS}
\definechangesauthor[name={Kevin}, color=blue]{KW}

\newcommand{\id}{\mathbb{I}}
\newcommand{\V}{\mathrm{V}}
\newcommand{\dd}{\mathrm{d}}
\newcommand{\ind}[1]{\mathbf{1}_{#1}}
\newcommand{\erf}{\;\mathrm{erf}}
\newcommand{\br}[1]{\mathopen{}\left( #1 \right)\mathclose{}}
\renewcommand{\exp}[1]{\mathrm{e}^{#1}}

\newcommand{\In}{\iota}%{\;\mathrm{in}}
\newcommand{\BS}{\beta}%\mathrm{BS}}
\newcommand{\FTBS}{\BS}%\mathrm{FTBS}}

\jname{rsta}
\Journal{Phil. Trans. R. Soc}

\begin{document}

%opening
\title{Finite-Time Basin Stability and Independence Times}

\author{Paul~Schultz$^{1,2}$, Frank~Hellmann$^{1}$, Kevin~Webster$^{1}$, J\"{u}rgen~Kurths$^{1,2,4,5}$}

%%%% Subject entries to be placed here %%%%
\subject{xxxxx, xxxxx, xxxx}

%%%% Keyword entries to be placed here %%%%
\keywords{xxxx, xxxx, xxxx}

%%%% Insert corresponding author and its email address}
\corres{Paul~Schultz\\
\email{pschultz@pik-potsdam.de}}

\address{$^1$Potsdam Institute for Climate Impact Research (PIK), Member of the Leibniz Association, P.O. Box 60 12 03, D-14412 Potsdam, Germany\\
$^2$Department of Physics, Humboldt University of Berlin, Newtonstr. 15, 12489 Berlin, Germany\\
%$^3$ sth else? \\
$^3$Institute for Complex Systems and Mathematical Biology, University of Aberdeen, Aberdeen AB24 3UE, United Kingdom\\
$^4$Department of Control Theory, Nizhny Novgorod State University, Gagarin Avenue 23, 606950 Nizhny Novgorod, Russia}

\begin{abstract}
We study the stability of deterministic systems given sequences of rare, large, jump-like perturbations.
To do so we define the notion of the independence time of a deterministic system as the time after which 
we can assume that a system has returned to its original attractor, following such a perturbation.
To determine the independence time, we in turn introduce the notion of finite-time basin stability.
This is the probability that a system will return within a given time. 
The independence time can then be determined as the time scale at which the finite-time basin stability 
equals the asymptotic basin stability. 
The effect of jump-like perturbations that occur at -- at least -- the independence time apart is 
thus completely described as a fixed probability to exit the basin of attraction at each perturbation. 
%with the exit probability given by the basin stability.
The rate at which the system exits the basin is given purely in terms of the asymptotic basin stability 
and the frequency of the perturbations.
\end{abstract}

\maketitle

\section{Introduction}\label{sec:intro}

A typical problem associated to multi-stable dynamical systems is to guarantee 
the stability of an attractor under repeated perturbations. 
On the one hand,if perturbations are small, stability can be assessed in terms of asymptotic
stability theory for linear systems \cite{Lyapunov1907}, 
e.g. by calculating Lyapunov exponents, or
by formulating the problem as a stochastic differential equation. 

On the other hand, for large perturbations, a typical approach is
to base stability on properties of the basin of attraction, for instance 
their size \cite{Wiley2006,Klinshov2015,Mitra2015}. For this, several direct and sampling-based
methods are available.  

Especially Lyapunov funtions \cite{Hahn1958,Malisoff2009,Giesl2015} 
and related concepts like nonequilibrium potentials \cite{Graham1984,Graham1991}
are powerful tools for approximating basins of attraction.
Furthermore, the existence of a global Lyapunov function ensures that 
a system will always return to the corresponding attractor. 

The explicit construction of Lyapunov functions for a given system is a difficult problem in general. 
However, several numerical approaches for the computation of Lyapunov functions have been developed, 
including the SOS (sums of squares) method \cite{Parrilo2000}, the CPA (continuous piecewise affine) 
method \cite{Hafstein2004}, and the numerical solution of Zubov's equation \cite{Camilli2001}. 
For a survey of these methods, see \cite{Giesl2015}.

Direct methods, however, are typically not efficient for high-dimensional systems
and yield conservative bounds on the attraction basin \cite{Chiang2010,Gajduk2014}
 
Basin stability $\BS$ \cite{Menck2013,Schultz2017} instead studies the probability 
that a system will return to an attractor following a large, jump-like perturbation. 
Like other measures designed this way (e.g. \cite{Rega2005,Hellmann2016}), 
it has the advantage of allowing for efficient estimators by sampling the phase space and the trajectories directly. 
These estimators have a sampling error that is independent of the system dimension. 
Thus $\BS$ can be efficiently evaluated for high-dimensional systems and for dynamics where no 
analytic Lyapunov functions are known.

In the following, we study the behavior of a system under repeated but rare perturbations. 
We answer the question how rare perturbations need to be in order for the results available for singular perturbations to apply.

An inescapable problem when studying the return of a system to an attractor lies in the fact that this return typically 
takes infinitely long and requires regularization \cite{Kittel2016}. Here we use the independence of subsequent 
perturbations as our return criterium. This allows us to define the independence time as the time after which we can 
expect a system to have returned sufficiently close to an attractor such that a subsequent perturbation is 
statistically independent. We say two perturbations are statistically independent iff the outcome of one perturbation event 
(i.e. the time-asymptotic behaviour) is not influenced by any preceeding event.

To efficiently evaluate the independence time we introduce the notion of finite-time basin stability $\FTBS\br{T}$. 
This is a finite-time horizon version of basin stability, corresponding to the probability that a system has returned to the attractor 
(according to a chosen criterion) in time $T$. By combining this with the return criterion required for independence time we can give 
a lower bound for the independence time as the time when the finite-time basin stability approaches its asymptotic value.
Furthermore, this enables us to derive an efficient estimator for the independence time for high-dimensional systems.

Given a set of perturbations that occur less frequent than the independence time, the probability to exit the basin of attraction 
is simply given in terms of the basin stability and the frequency of perturbations.
This is particularly of interest ff the asymptotic basin stability is unity for a given set of perturbations.
Then, the independence time is the time interval that has to pass between pertubations to ensure that an arbitrary 
sequence of such perturbations can not destabilize the system.

The paper is organised as follows: 

\section{Definitions} \label{sec:setup}

\subsection{The system}
We will consider an autonomous multi-stable dynamical system for which we can describe the dynamical 
evolution with a system of first-order ordinary differential equations, i.e.

\begin{align} \label{eq:system_no_pert}
\begin{split}
\dot{x} = f\br{x}\;,
\end{split}
\end{align}

with states $x$ living in a phase space $X\subseteq\mathbb{R}^n$. 
We are interested in the case that the system has at least one stable fixed point, 
which, without loss of generality, we assume to be at the origin $x^\ast = 0$, such that $f(x^\ast) = 0$. 
We denote the basin of attraction of the origin as $B \subset X$. Accordingly, the basin stability~\cite{Menck2013}
of the fixed point $x^\ast$ with respect to a probability density $\rho$ of perturbations is given by 

\begin{align} \label{eq:bs}
\begin{split}
\BS := \int\limits_X \ind{B}\br{x} \rho\br{x} ~d x ,\quad \BS \in [0;1] \;.
\end{split}
\end{align}

$\BS$ corresponds to the probability that the system -- initially at $x^\ast$ -- returns to the 
fixed point for a perturbation drawn from $\rho$. It is proportional to the basin volume if $\rho$ 
is chosen as a uniform probability density.

We now subject the system Eqn.~\ref{eq:system_no_pert} to a possibly infinite sequence of jump 
perturbations, with magnitude $\delta x_i$ drawn at random from a probability density $\rho(\delta x)$ and starting 
at time $t = 0$. We do not further specify the discrete times $t_i$ at which these perturbations 
occur, i.e. perturbations might appear regularly or according to some distribution. 
The minimum difference between subsequent perturbations will be denoted by $\Delta t = \min_i\br{t_i - t_{i + 1}}$. 
Initialising the system at the attractor, this setup leads to the stochastic integral equation

\begin{align} \label{eq:system}
\begin{split}
x(t) = \int_0^t\dd t f\br{x\br{t}}  + \int_0^t\dd t \sum_{i=0}^\infty  \delta x_i \delta\br{t - t_i} \;.
\end{split}
\end{align}

For convenience we introduce the number $n(t)$ of jumps that have happened at a time $t$:

\begin{align} \label{eq:number_of_jumps}
\begin{split}
n(t) = \int_0^t\dd t \sum_{i=0}^\infty \delta\br{t - t_i}\;.
\end{split}
\end{align}

If the jumps are sufficiently rare we expect that the probability for a solution $x\br{[0, t]}$ 
to Eqn.~\eqref{eq:system} to continuously remain in the basin of attraction up to time $t$ to be given by

\begin{align} \label{eq:remain-probability}
\begin{split}
p\br{x\br{[0, t]} \in B} \approx \BS^{n(t)}\;,
\end{split}
\end{align}

that is, every perturbation counted by $n(t)$ has an equal and constant probability to leave the system within
the basin of attraction (or for pushing it out). In Sec.~\ref{sec:independence} we will quantify what sufficiently 
rare means to achieve such a formula. Before, as an additional prerequisite, we turn to the definition of 
finite-time basin stability.

\subsection{Finite-Time Basin Stability} 

Our analysis is based on the return times of perturbed states within the basin of attraction $B$
to the original attractor, i.e. to $x^\ast$.  
To define return times in a robust way, we make use of Lyapunov functions.
A Lyapunov function is a function $V(x)$ with monotonous orbital derivative, i.e. 
it decreases along trajectories of \ref{eq:system_no_pert} and has a (local) minimum at the 
fixed point $x^\ast$ \cite{Lyapunov1907,Hahn1958,Malisoff2009}. If $V$ is minimal 
in $x^\ast$ and furthermore $\dot V\br{x}<0$ for all $x$ in a neighbourhood of 
$x^\ast$, the fixed point is asymptotically stable.
A time-tracking Lyapunov function $V$ is defined on $B$ and satisfies the differential equation:

\begin{align} 
\begin{split}
\frac{d}{dt} V\br{x(t)} = -1\;,
\end{split}
\end{align}

i.e., it strictly decreases along any trajectory of Eqn.~\eqref{eq:system}.
It is straightforward to see that the values of such Lyapunov functions track the time. 
If $x(t)$ and $x(t')$ are two points on the same trajectory, then by integrating the defining 
equation above we have:

\begin{align} 
\begin{split}
V(x(t)) - V(x(t')) = t - t'\;.
\end{split}
\end{align}

To fully determine such a Lyapunov function we need to specify boundary conditions on a transverse surface $S$. 
If we set $V(S) = 0$, the Lyapunov function measures how long it has been since, or will be until the system crosses the surface $S$. 
We denote this Lyapunov function as $V_S$. The set $S$ defines our return condition and the finite-time basin stability, 
given $\rho$ and $S$, is defined as:

\begin{align} \label{eq:ftbs}
\begin{split}
\BS(T) := \int\limits_X \ind{B}\br{x} \Theta\left(T - V_S(x)\right) \rho\br{x} ~d x \;\in [0;1]\;.
\end{split}
\end{align}

Where $\Theta$ is the heaviside function. This is the probability that a trajoctory, following a perturbation drawn from $\rho(x)$,
will return to within $S$ around the attractor $x=0$ within time $T$. For well-behaved vector fields $f(x)$ one expects that 
$\lim_{T \rightarrow \infty} \BS(T) = \BS$.


\section{Independence of Perturbations}\label{sec:independence}

We now turn to the key question: \emph{When do we consider the system to have returned?}
As noted above, we want ``returned'' to imply that a subsequent perturbation of the system is statistically independent from previous ones.
Let us define a distance function $\In\br{x,x'}$ on phase space as the $L^1$ norm of the difference of the shifted probability distributions 
$\rho_x\br{\cdot} = \rho\br{\cdot - x}$:

\begin{align} \label{eq:in}
\begin{split}
\In\br{x, x'} = \int\limits_X \left|\rho_x\br{u} - \rho_{x'}\br{u}\right| du\;.
\end{split}
\end{align}

Note that for an arbitrary state vector $y$, $\In\br{y, x}$ is subadditive, symmetric, non-negative 
and vanishes for $x=y$, hence it is a pseudometric on $X$. 
We will use the shorthand $\In\br{x} = \In\br{x, 0}$ for the distance to the fixed point.

The expectation value of some observable $\chi\br{x}$ satisfying $\left|\chi\br{x}\right| \leq 1$ 
with respect to the two distributions $\rho$ and $\rho_x$  differs at most by $\In\br{x}$:

\begin{align} 
\begin{split}
\left|\int\limits_X \chi\br{u} \rho\br{u}~du - \int\limits_X \chi\br{u} \rho_x\br{u}~du\right| 
% = \left|\int\limits_X \chi\br{u} \br{\rho\br{u} - \rho_x\br{u}}~du\right| \\
&\leq  \int\limits_X \left|\chi\br{u}\right| \left|\rho\br{u} - \rho_x\br{u}\right| ~du \\
&\leq \int\limits_X \left|\rho\br{u} - \rho_x\br{u}\right|~du = \In\br{x}
\end{split}
\end{align}

The probability to remain in the fixed point's basin of attraction after a perturbation originating at $x$ 
is given by the basin stability $\BS_x$ of the shifted probability density $\rho_x$:

\begin{align} \label{eq:bs_shifted}
\begin{split}
\BS_x := \int\limits_X \ind{B}\br{u} \rho_x\br{u} ~d u \;\in [0;1]
\end{split}
\end{align}

Both basin stability and finite-time basin stability are defined as the expectation value of the basin indicator function. 
Thus in particular we have that

\begin{align} 
\begin{split}
|\BS - \BS_x| \leq \In\br{x} \quad \text{and} \quad |\FTBS\br{T} - \BS_x(T)| \leq \In\br{x}\; .
\end{split}
\end{align}

Now, imagine the system Eqn.~\eqref{eq:system_no_pert} at two different jump events $t_i$ and $t_j$. The difference
in the expectation value for an observable (especially the basin indicator function) is bounded by a specific 
value of $\In\br{x(t_i), x(t_j)}$ that vanishes exactly in the case of statistical independence of perturbations.
Therefore, the distance to the attractor in our metric $ \In$ is a meaningful measure for the return to the attractor.

\section{Independence Times}

Given an $\epsilon > 0$, define the finite-time basin stability $\FTBS\br{T}$ with respect to a transversal surface  $S$
such that $\In\br{x} < \epsilon$ for all $x$ enclosed by $S$. Now given a threshold $\delta > 0$, we define the 
\emph{independence time} as the time $T_{ind}(\epsilon, \delta)$ such that:

\begin{align} \label{eq:independence_time}
\begin{split}
T_{ind}\br{\epsilon, \delta} := \inf \{ T > 0 \vert \BS - \FTBS\br{T} \leq \delta \} \;.
\end{split}
\end{align}

Thus we have quantified what rare means for our sequence of perturbations and the system Eqn.~\eqref{eq:system}. 
Given a sequence of perturbations drawn from $\rho$, occuring at times $t_i$ with minimum interval $\Delta t$ 
larger than the independence time $\Delta t > T_{ind}(\epsilon, \delta)$, the probability to stay within 
the basin of attraction, given that we start in  $S$ near the attractor, is bounded by:


\begin{align} \label{eq:remain-probability-epsilons}
\begin{split}
p_{surv}\br{t, x(0)} = p\br{x\br{[0, t]} \in B \vert x(0) \in S } \geq \br{\BS - \delta - \epsilon}^{n(t)}\;,
\end{split}
\end{align}

for all times.

To show this, let us consider the perturbed system Eqn.~\eqref{eq:system}. 
At each jump event $t_i$, the conditional probability to \emph{not} exit the basin of attraction 
is given as the shifted basin stability evaluated at the left limit $x_i$ of the 
trajectory before the jump:

\begin{align} 
\begin{split}
p\br{x(t_i^+) \in B \vert x(t_i^-) = x_i \in B} = \BS_{x_i}\;,
\end{split}
\end{align}

where $t_i^+$ and $t_i^-$ denote the right respectively left limit of $t$ to the jump time $t_i$.
Therefore, if we ensure that $\BS_{x_i}$ is close to $\BS$, 
we will also ensure that the perturbations are independent of each other.

Now given that the process is in $S$ before the jump at $t_i$, we want to understand what the probability 
is that it will return to $S$ before the next jump at $t_{i}$. If we started at the attractor rather 
than in $S$ this would be given by $\BS\br{\Delta t}$ with $\Delta t = t_{i} - t_{i-1}$. The probability 
with respect to the shifted probability density thus differs from this at most by $\epsilon$. 
Assuming further that $\Delta t$ is larger than the independence time $T_{ind}$, Eqn.~\eqref{eq:independence_time}
yields the lower bound:

\begin{align} 
\begin{split}
p\br{x(t_{i}^-) \in S| x(t_{i-1}^-) \in S} &\geq \BS\br{\Delta t} - \epsilon \geq \BS - \delta - \epsilon 
\end{split}
\end{align}

\begin{align} 
\begin{split}
p_{surv}\br{t, x(0)} 
&= \prod\limits_{i=1}^{n(t)} p\br{x(t_{i}^-) \in S| x(t_{i-1}^-) \in S} \\
&\geq \prod\limits_{i=1}^{n(t)} \BS - \delta - \epsilon \\
&= \br{\BS - \delta - \epsilon }^{n(t)}
\end{split}
\end{align}

The above formula applies as soon as the system enters the region bounded by $S$ once. 
Hence, Eqn.~\eqref{eq:remain-probability-epsilons} will also be the asymptotic form of the probability for general starting points in $B$
if a stochastic process conditioned on staying in the basin of attraction is ergodic, 
or more generally if $S$ has a finite hitting time. %the probability to not hit $S$ goes to zero. 
 

\section{A practical estimator} \label{sec:estimator}

\begin{figure}[ht!]
    \centering
    \begin{subfigure}[t]{0.5\textwidth}
        \centering
        \includegraphics[width=.8\columnwidth]{figures/schematic}
        \caption{}
    \end{subfigure}%
    ~ 
    \begin{subfigure}[t]{0.5\textwidth}
        \centering
        \includegraphics[width=.8\columnwidth]{figures/swing_escape_traj}
        \caption{}
    \end{subfigure}
    \caption{
    \textbf{(a)} 
    Schematic picture of the basin of attraction $B$ with boundary $\partial B$ 
    of a the fixed point $x^\ast$ in a phase space $X$, visualising the relation of the
    sets $U_\epsilon$ and $S_\epsilon$ defined in Sec.~\ref{sec:estimator}.
    In a multistable system, trajectories eather approach the fixed point or other 
    attractors, for instance a limit cycle $x^{LC}$.
    \textbf{(b)} 
    Example realisation of the stochastic process Eqn.~\ref{eq:system} with 
    %and initial conditions $\phi,\omega=(-1.0952533, 4.56724324)$
    %time interval $T=0.1$ between jumps, leading to an escape time of $T_e=6.9$.
    the deterministic part of the dynamics given Eq.~\ref{eq:swing}.
    The jump intervals are chosen to be comparatively short ($T=0.1$), hence 
    the trajectory quickly escapes the corresponding basin of attraction
    (orange area).
    }
   \label{fig:schematic}
\end{figure}


The above arguments establish a lower bound for the remain probability, but they 
do not provide an effective way to evaluate the quantities involved. The main 
difficulty in constructing an efficient estimator lies in evaluating the metric 
$\In\br{x}$ and constructing a transversal return surface $S$ given an $\epsilon$. 
This problem simplifies considerably in the important special case that $\epsilon$ 
is chosen small enough that we only need to evaluate $\In\br{x}$ close to the attractor. 
We will give an explicit formula based on the linearized dynamics for this case now.

First let us consider $\In\br{x}$. Near the origin we have

\begin{align} \label{eq:in_approx}
\begin{split}
\In\br{x} \simeq |x| \int\limits_X \left|\nabla \rho\br{x'}\right| dx' = \frac{|x|}{C_\rho}\;,
\end{split}
\end{align}

defining a constant $C_\rho$ which is independent of the dynamics and can be evaluated 
analytically for some simple $\rho$, like gaussian distributions, or numerically in general.

Thus all points inside the sphere $U_\epsilon = \{ x | \left|x\right| = \epsilon C_\rho \}$ 
satisfy $\In\br{x} \leq \epsilon$ (cf. Fig.~\ref{fig:schematic}a)). 
This sphere might not be transversal, hence are looking for a transversal surface $S_\epsilon$
entirely contained within $U_\epsilon$. As we are in a neighbourhood of the fixed point we can 
consider the linearized system associated to Eqn.~\ref{eq:system_no_pert} given by:

\begin{align} 
\begin{split}
\dot x(t) = \nabla f \cdot x(t) := J x(t)
\end{split}
\end{align}

If the Jacobian matrix $J$ is symmetric then $U_\epsilon$ is transversal, we can chose 
$S_\epsilon = U_\epsilon$ and are done.
To account for the general case, we can make use of quadratic Lyapunov functions 
$W\br{x} = x^\dagger \cdot Lx$ that satisfy $\dot W\br{x} = x^\dagger Q x$ with $Q$ 
symmetric and negative definite. Given $J$ and a choice of $Q$, we can find 
a Lyapunov function by solving the matrix equation

\begin{align} 
\begin{split}
% {\dot x}^\dagger \cdot Lx + x^\dagger \cdot L \dot x &= x^\dagger (J^\dagger L + L J) \dot x = x^\dagger Q x\\
J^\dagger L + L J & = Q
\end{split}
\end{align}

To find the maximum $\left|x\right|$, or equivalently $x^2$ reached on the level 
set of $W\br{x}$ we differentiate $\left|x\right|^2$ in the direction parallel to the 
level set and look for extrema. Take a derivative $\partial_v = v \cdot \partial$. 
Then we require $\partial_v W\br{x} = 0$ for the derivative to be tangential to the level set. 
An extremum on the level set thus satisfies the following set of equations:

\begin{align} 
\begin{split}
\forall v :\quad \partial_v x^2 = 2 v^\dagger x &= 0 \quad \text{s.t.:} \quad
v^\dagger L x + x^\dagger L v = 2  v^\dagger L x  = 0
\end{split}
\end{align}

where we have used that $L$ is symmetric.
We immediately see that for $L = 1$, when our level sets are spheres, every point is an extremum.
In general, it follows that as $x$ is orthogonal to all $v$, and the $v$ span the space orthogonal to to $Lx$, 
$x$ and $Lx$ need to be parallel. In this case, the extrema are in the eigendirections of $L$. 
The maximum $x_{max}^2$ for a given level set is achieved in the eigendirection to the smallest eigenvalue $\lambda$. 
Setting $x_{max}^2 = \epsilon$ we have $W(x_{max}) = \lambda x_{max}^\dagger x_{max} = \lambda \epsilon$. Thus the largest level set contained in $U_\epsilon = \{ x | \left|x\right| = \epsilon C_\rho \}$ is given by $x^\dagger L x = \lambda (\epsilon C_\rho)^2$.

Then the transverse surface $S_\epsilon$ is defined as

\begin{align} 
\begin{split}
S_\epsilon = \{ x | x^\dagger L x = \lambda (\epsilon C_\rho)^2 \} \; .
\end{split}
\end{align}


The fact that we have $L$ on the left and $\lambda$ on the right shows that this relation does not depend on an overall 
scaling factor of the Lyapunov function. To make $S_\epsilon$ as large as possible we want to make the ratios 
$\frac{\lambda}{\lambda_i}$ small.

\added[id=FH]{Kevin can you have a look at finding the optimal $Q$ such that the Lyapunov function is as spherical as possible? I think that means making sure that the gap between the largest and the smalles eigenvalue is small. Thus we would look for a Q that minimizes $\left|L - \lambda 1\right|$. Both $L$ and $\lambda$ depend on $Q$, the first linearly, the second not so much...}

\section{Numerical Results} \label{sec:results}

There are basically two different practical approaches to estimate the finite-time basin
stability $\FTBS\br{\cdot}$. Given that a Lyapunov function $V$ and its level sets 
(cf. Sec.~\ref{sec:setup}) are known and specified, Eqn.~\ref{eq:ftbs} can be directly evaluated as

\begin{align} \label{eq:ftbs_b}
\begin{split}
\FTBS\br{T} = \oint_{V^{-1}(T)_0} x \wedge dx %= \oint_{V^{-1}(T)_0} x_0 dx_1 + x_1 dx_0\; . 
\end{split}
\end{align}

where we evaluate the integral only along level sets of $V$ that are entirely contained within
the basin of attraction of $x^\ast$. By $V^{-1}(T)_0$, we denote the component of the level 
set of $V$ at value $T$ that surrounds $x^\ast$. 
This integral corresponds to the phase space volume (as measured by a uniform distribution 
$\rho$) contained in the level sets $V^{-1}(T)_0$ and can be directly evaluated.

In most cases, especially in high-dimensional systems, a suitable Lyapunov function can not
be determined, but the integral Eqn.~\ref{eq:ftbs} can also be efficiently evaluated using a
Monte-Carlo sampling approach.

In the following, we compare both approaches using a two-dimensional test system, 
namely the damped-driven pendulum
%We numerically determine $\FTBS$ and critical times $T_c\br{\epsilon, \epsilon'}$

\begin{align} \label{eq:swing}
\begin{split}
\dot\phi &= \omega\\
\dot\omega &= p - \alpha\omega - k \sin\br{\phi + \arcsin\frac{p}{k}}
\end{split}
\end{align}

with $p=1$, $\alpha=0.1$ and $k=8$. For this set of parameters, the system has
two attractors, namely a limit cycle and a fixed point 
$x^\ast = \br{\phi^\ast,\omega^\ast} = \br{0, 0}$ at the origin. Note that we applied a 
global phase shift of $\arcsin\br{p/k}$ to achieve this. 

For illustrative purposes, we choose a distribution $\rho\br{\phi, \omega}$ 
to draw uniformly distributed perturbations at a point $x=\br{\phi, \omega}$ 
from the box $R=\left[\phi-\pi;\phi+\pi\right]\times\left[\omega-10;\omega+10\right]$.
This way, $R$ is almost entirely overlapping the basin of attraction of $x^\ast$,
such that we can expect $\BS$ to be close to $1$. Still, $\FTBS\br{T}$ can 
deviate strongly from $\BS$, especially for small $T$.

For the system Eqn.~\ref{eq:swing} a Lyapunov function is actually known.
However, as this is generally not the case, we numerically determine $V$,
making use of an radial basis function (RBF) approximation, which is a 
special case of mesh-free collocation \cite{Giesl2007}.  

The sampling procedure is as follows: 
\begin{itemize}
\item[Prerequisites] Choose a distribution $\rho$ and a tolerance $\epsilon$. Determine $S_\epsilon$, for instance using the method described in Sec.~\ref{sec:estimator}.
\item[Sampling step]
	\begin{enumerate}
	\item Draw a random initial condition from $\rho_{x^\ast}$ centered at the fixed point.
	\item Integrate the unperturbed system (Eqn.~\ref{eq:system_no_pert}) and record if the 
	trajectory returns to $x^\ast$ and the return time $T$ to $S_\epsilon$ if applicable.
	\end{enumerate}
\end{itemize}

The sampling step should be repeated for a sufficient ensemble of initial conditions
to get significant statistics (we use a sample size of 20000 points). Denote by $M_T$
the number of trajectories returning to $S_\epsilon$ within time $T$ or less and by
$N_{x^\ast}$ the number of all trajectories starting in the basin of attraction of $x^\ast$.
Then, an estimator for finite-time basin stability is given by

\begin{align} \label{eq:ftbs_est}
\begin{split}
\hat{\FTBS}\br{T} = \frac{M_T}{N_{x^\ast}} 
\end{split}
\end{align}

with a standard error $e_{\hat{\FTBS}\br{T}}$ as

\begin{align} \label{eq:ftbs_est_error}
\begin{split}
e_{\hat{\FTBS}\br{T}} = \sqrt{\frac{\hat{\FTBS}\br{T} \br{1 - \hat{\FTBS}\br{T}}}{N_{x^\ast}}} \; ,
\end{split}
\end{align}

since for a fixed $T$ trajectories either return or not and we can regard this as a Bernoulli experiment.
Note that if $\hat{\FTBS}\br{T}\approx 1$ or $\hat{\FTBS}\br{T}\approx 0$, more robust estimators are 
available \cite{Agresti1998}.

\begin{figure}[ht!]
    \centering
    \begin{subfigure}[t]{0.5\textwidth}
        \centering
        \includegraphics[width=\columnwidth]{figures/swing_ftbs}
        \caption{}
    \end{subfigure}%
    ~ 
    \begin{subfigure}[t]{0.5\textwidth}
        \centering
        \includegraphics[width=\columnwidth]{figures/swing_Tc}
        \caption{}
    \end{subfigure}
    \caption{
    \textbf{(a)} 
    $\FTBS$ curve.
    We are actually plotting two curves using Eqn.~\ref{eq:ftbs} and Eqn.~\ref{eq:ftbs_b}.
    \textbf{(b)} 
    $T_{ind}$
    }
   \label{fig:ftbs_curve}
\end{figure}

Fig.~\ref{fig:ftbs_curve}a) summarizes the results for the system Eqn.~\ref{eq:swing} obtained both 
from numerically constructing a Lyapuno function as well as the Monte-Carlo sampling. The horizontal
line denotes the the basin stability estimation $\hat\beta=0.9874$ which is close to $1$ as expected due to our choice 
of $R$. Indeed, after a certain time depending on $\epsilon$, we observe that the finite-time basin 
stability curves approach the value of $\BS$. From these points, we determine the independence times
$T_{ind}\br{\epsilon,\delta}$ depicted in Fig.~\ref{fig:ftbs_curve}b) using Eqn.~\ref{eq:independence_time}.




\begin{figure}[ht!]
    \centering
    \begin{subfigure}[t]{0.5\textwidth}
        \centering
        \includegraphics[width=\columnwidth]{figures/swing_psurv}
        \caption{}
    \end{subfigure}%
    ~ 
    \begin{subfigure}[t]{0.5\textwidth}
        \centering
        \includegraphics[width=\columnwidth]{figures/swing_psurv_asymp}
        \caption{}
    \end{subfigure}
    \caption{
    \textbf{(a)} 
    \textbf{(b)} 
    }
   \label{fig:surv}
\end{figure}

\section{Discussion} \label{sec:discussion}

Just as for asymptotic basin stability, finite-time basin stability admits a simple and efficient sampling-based estimator 
that works for systems with a high number of dimensions.
In the special case of the asymptotic basin stability being equal to one, this allows us to effectively guarantee 
(up to sampling errors), that perturbations that occur at least the independence time apart can not destabilize a system.

So far, we only discussed ficed points and left higher-dimensional attractors aside as it might not be possible to 
define a suitable Lyapunov function and determine return times. 

We can mention the interpretation of basin stability as an escape rate here.

\section{Conclusion}

\enlargethispage{20pt}

\ethics{No animals have been harmed in the research of this paper.}

\dataccess{Insert details of how to access any supporting data here.}

\aucontribute{For manuscripts with two or more authors, insert details of the authors’ contributions here. This should take the form: 'AB caried out the experiments. CD performed the data analysis. EF conceived of and designed the study, and drafted the manuscript All auhtors read and approved the manuscript'.}

\competing{The authors declare that they have no competing interests.}

\funding{PS, FH and JK acknowledge the support of BMBF, CoNDyNet, FK. 03SF0472A. All authors 
gratefully acknowledge the European Regional Development Fund (ERDF), the German Federal 
Ministry of Education and Research and the Land Brandenburg for supporting this project by 
providing resources on the high performance computer system at the Potsdam Institute for Climate Impact Research.}

\ack{Coffee.}

\disclaimer{Insert disclaimer text here if applicable.}


%\nocite{*}
\bibliographystyle{rsta}
\bibliography{lib}




\end{document}

import os, sys
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d.axes3d import Axes3D
from matplotlib import cm
from matplotlib import rcParams
import shutil

#to find geninterp
current_path = os.getcwd()
current_path_list = current_path.split('/')
module_path_list = current_path_list[:-1]
module_path = '/'.join(module_path_list)
sys.path.append(module_path)

from core import MeshData, OrbDerivInterpolant
from kernel import *

basepath = os.getcwd()  # /Users/kevinwebster/Dropbox/PyCharm Projects/COPAN Lyapunov fn2

basepath_list = basepath.split("/")

path_list = basepath_list + ['Data']

class COPANio(object):

    def __init__(self, Int_obj):
        self.path_already_exists = False
        self.identifier = Int_obj.identifier
        self.Int_obj = Int_obj
        assert isinstance(self.Int_obj, OrbDerivInterpolant)

        # Second Data object would have already been removed in COPANInterpolant initialiser if it is empty
        if len(self.Int_obj.data_points)==1:
            self.datapath_list = path_list + ['Unmixed Interpolation']
        else:
            self.datapath_list = path_list + ['Mixed Interpolation']

        if isinstance(self.Int_obj.data_points[0], MeshData):
            self.datapath_list = self.datapath_list + ['MeshData', self.identifier]
        else:
            self.datapath_list = self.datapath_list + ['Scattered Data', self.identifier]

        self.dir = '/'.join(self.datapath_list)         #folder to save data
        if not self.get_path(): # path didn't already exist
            self.save_initialisation()
        else:
            self.path_already_exists = True # Flag to indicate to COPANInterpolant to load data

    def load_stored_data(self):
        identifier, self.Int_obj.orb_deriv_RHS, range_tup_list, gram_load, coefficients, kernel_load = load(self.dir)

    def save_initialisation(self):
        #Save kernel
        kernelfile_path = self.dir + '/kernel.txt'
        kernelfile = open(kernelfile_path, 'w')
        kernelfile.write(str(self.Int_obj.K))
        kernelfile.close()

        #Save data object list
        for l, data_obj in enumerate(self.Int_obj.data_points):
            datafile_path = self.dir + '/datapts' + str(l)
            np.save(datafile_path, self.Int_obj.data_points[l].points)
            targetsfile_path = self.dir + '/targets' + str(l)
            np.save(targetsfile_path, self.Int_obj.data_points[l].targets)
            if isinstance(data_obj, MeshData):
                meshdatafile_path = self.dir + '/MeshDatarange' + str(l)
                np.save(meshdatafile_path, np.array(self.Int_obj.data_points[l].range_tup_list))

    def get_path(self):
        returnval = True
        if not os.path.exists(self.dir):
            returnval = False
            os.makedirs(self.dir)
        return returnval

    def save_data(self, del_gram_rows=True):
        #Save gram matrix
        gramfile_path = self.dir + '/gram'
        np.save(gramfile_path, self.Int_obj.gram)
        self.Int_obj._check_gram(self.Int_obj.gram)
        if del_gram_rows==True:
            if os.path.exists(self.dir + '/Gram matrix'):
                #Delete stored rows in Gram matrix folder, since we have the full matrix saved
                shutil.rmtree(self.dir + '/Gram matrix')

        #Save coeffs
        coeffsfile_path = self.dir + '/coeffs'
        np.save(coeffsfile_path, self.Int_obj.coefficients)

    def plot_fig(self, fig_name, x, y, s=None, f=None, cont=True, Lyap=True):
        """
        Plots either Lyapunov function approximation or quiver plot
        :param fig_name: Name of file to be saved
        :param s: Must be set to Lyapunov fn to produce surf plot
        :param f: Must be set to RHS of vector field to produce quiver plot
        :param Quiver: Option to also plot contour (s param must be set)
        :return: None
        """

        plt.rc('lines', linewidth=2)
        #plt.rc('font', family='Arial', size=40, weight='bold')
        plt.rc('axes', linewidth=2, titlesize='small', labelsize='40', unicode_minus=True)
        plt.rc('xtick', labelsize='15', direction='in')
        plt.rc('xtick.major', size=5, width=2)
        plt.rc('ytick', labelsize='15', direction='in')
        plt.rc('ytick.major', size=5, width=2)
        #plt.rc('legend', fancybox=True, fontsize='large', markerscale=1)
        plt.rc('contour', negative_linestyle='dashed')
        #plt.rc('figure', figsize=(11.69, 8.27))
        plt.rc('savefig', bbox='tight', dpi=100, jpeg_quality=100)
        plt.rc('text', usetex=True)


        # colourmap with ten entries, good for printing and colourblind-friendly
        self.colours = np.array(['#a6cee3', '#1f77b4', '#b2df8a', '#33a02c', '#fb9a99', '#e31a1c', '#fdbf6f', '#ff7f00', '#cab2d6', '#6a3d9a'])

        fig_path = self.dir + '/Figures'
        if not os.path.exists(fig_path):
            os.makedirs(fig_path)
        fig_path = fig_path + '/' + fig_name

        if s is not None:
            # plotLrange = np.arange(0.05, 1, 0.005)
            # plotPrange = np.arange(0, 1, 0.005)
            plotLgrid, plotPgrid = np.meshgrid(x, y)

            W = s(np.array([plotLgrid.T, plotPgrid.T]).T)   #Interpolant evaluation on the grid

            if Lyap==True:
                # Surf plot
                fig = plt.figure()
                ax = fig.add_subplot(1, 1, 1, projection='3d')

                #ax.tick_params(axis='both', which='major', labelsize=15)
                #ax.tick_params(axis='both', which='minor', labelsize=15)

                ax.plot_surface(plotLgrid, plotPgrid, W, rstride=1, cstride=1, cmap=cm.coolwarm,
                                       linewidth=0, antialiased=False)
                ax.view_init(elev=51., azim=-134)
                plt.xlabel(r'$\phi$')
                plt.ylabel(r'$\omega$')


                fig.savefig(fig_path + 'surf' + '.pdf')

                #plt.savefig(fig_path + 'surf' + '.pdf', format="pdf")
                plt.close(fig)

            if cont==True:
                # Contour plot
                fig = plt.figure()

                cs = plt.contour(plotLgrid, plotPgrid, W)
                plt.clabel(cs, inline=1, fontsize=10)

                plt.xlabel(r'$\phi$')
                plt.ylabel(r'$\omega$')

                plt.savefig(fig_path + 'contour' + '.pdf', format="pdf")
                plt.close(fig)

        if f is not None:
            # Phase portrait plot
            fig = plt.figure()
            plotLgrid, plotPgrid = np.meshgrid(x, y)

            Q =  f(np.array([plotLgrid.T, plotPgrid.T]).T)

            plt.quiver(plotLgrid, plotPgrid, Q[...,0], Q[...,1])

            plt.xlabel(r'$\phi$')
            plt.ylabel(r'$\omega$')

            plt.savefig(fig_path + 'quiver' + '.pdf', format="pdf")

            plt.close(fig)

        #plt.show()
        plt.close('all')

    def _checkdir(self, dir):
        if not os.path.exists(dir):
            print("ERROR: \"" + dir + "\" does not exist!")
            exit(1)



def load(path):
    load_path_list = path.split('/')

    """
    base_path = os.getcwd()
    base_path_list = base_path.split('/')
    full_path_list = base_path_list + ['Data'] + load_path_list
    full_path = '/'.join(full_path_list)
    """
    full_path = path

    if not os.path.exists(full_path):
        print("ERROR: \"" + full_path + "\" does not exist!")
        exit(1)


    identifier = load_path_list[-1]
    orb_deriv_RHS = load_path_list[-1][-1]

    print('orbderivrhs = ', orb_deriv_RHS)
    print('Identifier = ', load_path_list[-1])

    range_tup_list = []
    if load_path_list[-3] == 'Mixed Interpolation':   #data_list will have 1 Data object
        if load_path_list[-2] == 'MeshData':            #that Data object is MeshData
            l=0
            if os.path.exists(full_path + '/MeshDatarange' + str(l) + '.npy'):
                range_tup_list = [tuple(range_tup) for range_tup in np.load(full_path + '/MeshDatarange' + str(l) + '.npy')]
                #l+=1
            else:
                print('Cannot load: ', full_path + '/MeshDatarange' + str(l) + '.npy does not exist!')
                exit(1)
        """
        #Not needed - targets are generated when MeshData is called
        orb_deriv_targets=None
        if os.path.exists(full_path + '/targets0.npy'):
            orb_deriv_targets = np.load(full_path + '/targets0.npy')
        """
        gram = None
        dict_of_gram_blocks = {}
        if os.path.exists(full_path + '/gram.npy'):
            gram = np.load(full_path + '/gram.npy')[()]     # Hack to load sparse matrix

        coefficients = None
        if os.path.exists(full_path + '/coeffs.npy'):
            coefficients = np.load(full_path + '/coeffs.npy')
        if os.path.exists(full_path + '/kernel.txt'):
            kernel_file = open(full_path + '/kernel.txt')
            K = eval(kernel_file.read())
        return identifier, orb_deriv_RHS, range_tup_list, gram, coefficients, K
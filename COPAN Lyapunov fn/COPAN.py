
import sys, os
import numpy as np
from core import OrbDerivInterpolant, MeshData, Data, gram_Wendland_sparse
from kernel import Wendland, Kernel
import COPANio as io
from model import COPAN, CONDYNET


# model = COPAN()
model = CONDYNET()

def f(x):
    return model.RHS(x)

##################
# determine return set S_e

phi_min, phi_max = -0.025, 0.025
omega_min, omega_max = -.1, .1
grid_size_x, grid_size_y = 0.001, 0.001
x_range = (phi_min, phi_max, grid_size_x)
y_range = (omega_min, omega_max, grid_size_y)
x_val = np.arange(*x_range)
y_val = np.arange(*y_range)

Crho =  1. / omega_max + 1. / phi_max # uniform dist.
e = 1e-3

def set_lin_matrices(par, point):
    # derive matices for linear system
    from scipy.linalg import solve_lyapunov, eigvals
    jacobian = np.array([[0., 1.], [- par["k"] * np.cos(point[0] + par["shift"]), - par["alpha"]]])
    lyapunov = solve_lyapunov(jacobian.transpose(), - np.diag(np.ones(2)))
    lambda_min = np.real(eigvals(lyapunov)).min()
    return lyapunov, lambda_min


def lyapunov_prod(lyapunov, x, p=[0, 0]):
    # x centered around p (i.e. fix point)
    # x[0] = np.mod(x[0], 2 * np.pi)
    return (x - p).dot(lyapunov.dot(x - p))

def set_Se(x, e, Crho, lyapunov, lambda_min):
    return lyapunov_prod(lyapunov, x) - lambda_min * e * e * Crho * Crho

lyapunov, lambda_min = set_lin_matrices(model.params, np.zeros(2))

res = np.zeros([len(np.arange(*x_range)), len(np.arange(*y_range))])

for i, x in enumerate(x_val):
    for j, y in enumerate(y_val):
        res[i, j] = set_Se(np.array([x, y]), e, Crho, lyapunov, lambda_min)

import matplotlib.pyplot as plt

xy = np.where(np.isclose(res, 0., atol=0.1*e))

plt.contourf(np.arange(*x_range), np.arange(*y_range), res.T)
plt.colorbar()
plt.scatter(x_val[xy[0]], y_val[xy[1]])
plt.show()

level_set = np.c_[x_val[xy[0]], y_val[xy[1]]]

print(level_set.shape)

##################

C = -1  # Constant orbital derivative

def p(x):
    assert isinstance(x, np.ndarray)
    #assert 2 == x.shape[-1]
    dim_axis = x.ndim - 1
    return -(np.linalg.norm(x, axis=dim_axis))**2

def c(x):
    assert isinstance(x, np.ndarray)
    return C * np.ones(x.shape[0:-1])


class COPANInterpolant(OrbDerivInterpolant):

    def __init__(self, *args, load_path=None):
        """

        :param args: data_list, Ker, func, orb_deriv_RHS  (if load_path==None)
        :param load_path: subfolder to be loaded e.g. 'Unmixed Interpolation/MeshData/Lstep=0_4Pstep=0_4orbderiv=c'
        :return:
        """
        if load_path is None:
            assert len(args)==4
            super().__init__(args[0], args[1], args[2])
            super()._check_for_empty_Data()
            assert args[3] in ['c', 'p']
            self.orb_deriv_RHS = args[3]     # used when saving data
            if isinstance(args[0][0], MeshData):
                self.grid = args[0][0].range_tup_list
                self.identifier = 'Lstep=' + str(args[0][0].range_tup_list[0][2]).replace('.', '_') + \
                                  'Pstep=' + str(args[0][0].range_tup_list[1][2]).replace('.', '_') + \
                                  'orbderiv=' + self.orb_deriv_RHS

            self.io = io.COPANio(self)
        else:
            #TODO: Currently only for Unmixed interpolation MeshData objects
            print('Loading data for', str(load_path.split('/')[-1]) + '...')
            identifier, self.orb_deriv_RHS, range_tup_list, gram_load, coefficients, kernel_load \
                = io.load(load_path)
            vector_field = f      # COPAN model above
            if self.orb_deriv_RHS=='c':
                func = c            # c function above
            elif self.orb_deriv_RHS=='p':
                func = p            # p function above
            else:
                print("Couldn't set orb_deriv_RHS to 'c' or 'p'")
                exit(1)
            if not isinstance(kernel_load, Kernel):
                print("Couldn't load kernel")
                exit(1)
            dummypts = Data()
            super().__init__([MeshData(*range_tup_list, function=func), dummypts], kernel_load, vector_field)
            super()._check_for_empty_Data()
            self.identifier = identifier
            self.coefficients = coefficients
            if gram_load is None:
                self.gram = gram_Wendland_sparse(self, load_path + '/Gram matrix')
            else:
                self.gram = gram_load
            self.io = io.COPANio(self)

    def build_gram(self):
        if not os.path.exists(self.io.dir + '/Gram matrix'):
            os.mkdir(self.io.dir + '/Gram matrix')
        self.gram = gram_Wendland_sparse(self, self.io.dir + '/Gram matrix')


# Lmin, Lmax = 0.01, 1
# Pmin, Pmax = 0.01, 1

# Lmin, Lmax = -np.pi / 2., np.pi / 2.
# Pmin, Pmax = -5., 5.

Lmin, Lmax = -np.pi, np.pi
Pmin, Pmax = -3., 3.

grid_sizeL, grid_sizeP = 0.2, 0.2

K = Wendland(3, 1, c=0.1/min(grid_sizeL, grid_sizeP))  # This choice of c gives a support that covers a 10-point radius

Lrange = (Lmin, Lmax, grid_sizeL)
Prange = (Pmin, Pmax, grid_sizeP)

datapts = MeshData(Lrange, Prange, function=c)

print('Number of data points: ', datapts.numpoints)

# The OrbDerivInterpolant object requires a list of 2 Data objects in the init. This script does not make use
# of function value specification, i.e. it only specifies orbital derivatives at given points. Therefore we
# need to pass in a dummy (empty) Data object
# dummypts = Data()

# Alternatively, we can use the following Data object to fix the interpolant to be equal to 0 at the origin

def unit_circle(npoints=10):
    angles = np.linspace(0, 2. * np.pi, npoints)
    return np.c_[np.cos(angles), np.sin(angles)]

offsetpt = Data()
offsetpt.set_data(level_set)
offsetpt.set_targets(np.zeros(level_set.shape[0]))

try:
    # Run the following lines to load stored data (Unmixed interpolation only for now, i.e. use dummypts above)
    load_path = os.getcwd() + '/Data/Unmixed Interpolation/MeshData/Lstep={}Pstep={}orbderiv=c'.format(grid_sizeL, grid_sizeP).replace(".", "_")
    inter_orbderiv = COPANInterpolant(load_path=load_path)
except:
    # Input second item in list as dummypts (no value interpolation) or offsetpt (forces value 0 at origin)
    inter_orbderiv = COPANInterpolant([datapts, offsetpt], K, f, 'p')
    inter_orbderiv.build_gram()

if inter_orbderiv.coefficients is None:
    # Linear system hasn't been solved yet
    inter_orbderiv.solve_linear_system(A=inter_orbderiv.gram, save_Gram_rows=[True, inter_orbderiv.io.dir])

def s_orbderiv(x):
    return inter_orbderiv.eval(x)


inter_orbderiv.io.save_data()
inter_orbderiv.io.plot_fig('fig', np.arange(*Lrange), np.arange(*Prange), s=s_orbderiv)

# inter_load = COPANInterpolant(load_path='Unmixed Interpolation/MeshData/Lstep=0_4Pstep=0_4orbderiv=c')

## calculate contour lines

def area(vs):
    a = 0
    x0,y0 = vs[0]
    for [x1, y1] in vs[1:]:
        dx = x1 - x0
        dy = y1 - y0
        a += 0.5 * (y0 * dx - x0 * dy)
        x0 = x1
        y0 = y1
    return a

x, y = np.meshgrid(np.arange(*Lrange), np.arange(*Prange))
W = s_orbderiv(np.array([x.T, y.T]).T)
levels = np.linspace(0, W.max(), 11)

import matplotlib.pyplot as plt
fig = plt.figure()
cs = plt.contour(x, y, W, levels=levels)
plt.clabel(cs, inline=1, fontsize=10)
plt.show()

norm = W.shape[0] * W.shape[1]
for i, contour in enumerate(cs.collections):
    vs = contour.get_paths()[0].vertices
    # Compute area enclosed by vertices.
    a = area(vs)
    print("r = " + str(levels[i]),  " a =" + str(a))
    print("normed", 1. * np.sum(W < levels[i]) / norm)


import os
import numpy as np

from model import CONDYNET

from core import OrbDerivInterpolant, MeshData, Data, gram_Wendland
from kernel import Wendland
import COPANio as io

def p(x):
    assert isinstance(x, np.ndarray)
    #assert 2 == x.shape[-1]
    dim_axis = x.ndim - 1
    return -(np.linalg.norm(x, axis=dim_axis))**2

def c(x):
    assert isinstance(x, np.ndarray)
    return - np.ones(x.shape[0:-1])

# model = COPAN()
model = CONDYNET()

def f(x):
    return model.RHS(x)


##################
# determine return set S_e

phi_min, phi_max = -0.025, 0.025
omega_min, omega_max = -.1, .1
grid_size_x, grid_size_y = 0.001, 0.001
x_range = (phi_min, phi_max, grid_size_x)
y_range = (omega_min, omega_max, grid_size_y)
x_val = np.arange(*x_range)
y_val = np.arange(*y_range)

Crho =  1. / omega_max + 1. / phi_max # uniform dist.
e = 1e-3

def set_lin_matrices(par, point):
    # derive matices for linear system
    from scipy.linalg import solve_lyapunov, eigvals
    jacobian = np.array([[0., 1.], [- par["k"] * np.cos(point[0] + par["shift"]), - par["alpha"]]])
    lyapunov = solve_lyapunov(jacobian.transpose(), - np.diag(np.ones(2)))
    lambda_min = np.real(eigvals(lyapunov)).min()
    return lyapunov, lambda_min


def lyapunov_prod(lyapunov, x, p=[0, 0]):
    # x centered around p (i.e. fix point)
    # x[0] = np.mod(x[0], 2 * np.pi)
    return (x - p).dot(lyapunov.dot(x - p))

def set_Se(x, e, Crho, lyapunov, lambda_min):
    return lyapunov_prod(lyapunov, x) - lambda_min * e * e * Crho * Crho

lyapunov, lambda_min = set_lin_matrices(model.params, np.zeros(2))

res = np.zeros([len(np.arange(*x_range)), len(np.arange(*y_range))])

for i, x in enumerate(x_val):
    for j, y in enumerate(y_val):
        res[i, j] = set_Se(np.array([x, y]), e, Crho, lyapunov, lambda_min)

import matplotlib.pyplot as plt

xy = np.where(np.isclose(res, 0., atol=0.1*e))

plt.contourf(np.arange(*x_range), np.arange(*y_range), res.T)
plt.colorbar()
plt.scatter(x_val[xy[0]], y_val[xy[1]])
plt.show()

level_set = np.c_[x_val[xy[0]], y_val[xy[1]]]

print(level_set.shape)
##################

phi_min, phi_max = -np.pi, np.pi
omega_min, omega_max = -3., 3.
grid_size_x, grid_size_y = 0.1, 0.1
x_range = (phi_min, phi_max, grid_size_x)
y_range = (omega_min, omega_max, grid_size_y)
datapts = MeshData(x_range, y_range, function=c)


print('Number of data points: ', datapts.numpoints)

# specify points to fix function values

# dummy points
def circle(r=1., npoints=10):
    angles = np.linspace(0, 2. * np.pi, npoints)
    return np.c_[r * np.cos(angles), r * np.sin(angles)]

offsetpt = Data()
offsetpt.set_data(level_set)
offsetpt.set_targets(np.zeros(level_set.shape[0]))

# set up interpolant
try:
    #import bla # force recompute
    load_path = os.path.join(os.getcwd(), "Data/", "Mixed Interpolation", "MeshData",
                             "Lstep={}Pstep={}".format(grid_size_x, grid_size_y).replace(".", "_"))
    identifier, orb_deriv_RHS, range_tup_list, gram, coefficients, K = io.load(load_path)
    inter_orbderiv = OrbDerivInterpolant([MeshData(*range_tup_list, function=c), offsetpt], K, f)
    inter_orbderiv.identifier = identifier
    inter_orbderiv.coefficients = coefficients
    inter_orbderiv.gram = gram
    print("load stuff")
except:
    # specify kernel
    K = Wendland(3, 1, c=0.1 / min(grid_size_x, grid_size_y))  # This choice of c gives a support that covers a 10-point radius
    inter_orbderiv = OrbDerivInterpolant([datapts, offsetpt], K, f)
    inter_orbderiv._check_for_empty_Data()
    inter_orbderiv.identifier = 'Lstep=' + str(datapts.range_tup_list[0][2]).replace('.', '_') + 'Pstep=' + str(datapts.range_tup_list[1][2]).replace('.', '_')
    # build gram matrix
    inter_orbderiv.gram = gram_Wendland(inter_orbderiv)

def s_orbderiv(x):
    return inter_orbderiv.eval(x)

inter_orbderiv.io = io.COPANio(inter_orbderiv)
if not os.path.exists(inter_orbderiv.io.dir + '/Gram matrix'):
    os.mkdir(inter_orbderiv.io.dir + '/Gram matrix')

inter_orbderiv.solve_linear_system(A=inter_orbderiv.gram, save_Gram_rows=[True, inter_orbderiv.io.dir])

inter_orbderiv.io.save_data()
inter_orbderiv.io.plot_fig('fig', np.arange(*x_range), np.arange(*y_range), s=s_orbderiv)

## calculate contour lines

def area(vs):
    a = 0
    x0,y0 = vs[0]
    for [x1, y1] in vs[1:]:
        dx = x1 - x0
        dy = y1 - y0
        a += 0.5 * (y0 * dx - x0 * dy)
        x0 = x1
        y0 = y1
    return a

x, y = np.meshgrid(np.arange(*x_range), np.arange(*y_range))
W = s_orbderiv(np.array([x.T, y.T]).T)
levels = np.linspace(0, W.max(), 11)

fig = plt.figure()
cs = plt.contour(x, y, W, levels=levels)
plt.clabel(cs, inline=1, fontsize=10)

norm = W.shape[0] * W.shape[1]
areas = []
for i, contour in enumerate(cs.collections):
    try:
        vs = contour.get_paths()[0].vertices
        # Compute area enclosed by vertices.
        a = area(vs)
        areas.append(a)
        print("r = " + str(levels[i]),  " a =" + str(a))
        print("normed", 1. * np.sum(W < levels[i]) / norm)
    except:
        pass

fig2 = plt.figure()
plt.plot(levels, np.array([1. * np.sum(W < l) / norm for l in levels]))
plt.plot(levels, areas)

plt.show()
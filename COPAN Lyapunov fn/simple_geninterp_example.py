import matplotlib.pyplot as plt
from geninterp.geninterp.core import *

"""
This script provides a simple example of the usage of the geninterp_sources module.
It computes a function which has function values specified at certain points, and also has
specified gradients at certain points.

This highlights the basic use of generalised interpolation: to compute interpolants that solve
'traditional' regression problems where function values are supplied at given grid points, as well
as problems where the interpolant needs to satisfy gradient conditions at given grid points.

More generally, we might want to specify a linear functional lambda, such that the interpolant f is required
to satisfy lambda(f)(x_i) = b_i, where x_i are given datapoints, and b_i are given values. The linear functional
could be the evaluation functional in the most trivial case, or the derivative operator followed by evaluation.
"""

# Load the kernel that is to be used for the interpolation. In all examples I use the Wendland kernel. This
# takes two arguments as input (l, k). Just choose l = floor(d/2) + k + 1 (where d is the data space dimension),
# then the kernel K will have smoothness C^{2k}
K = Wendland(3, 2)

# These points will have specified function values
testpts = Data()
testpts.set_data(np.array([[0], [1], [2]]))
testpts.set_targets(np.array([1, 10, -1]))

# These points will have specified function derivatives
testpts_derivs = Data()
testpts_derivs.set_data(np.array([[1], [2]]))
testpts_derivs.set_targets(np.array([0, 0]))

# The OrbDerivInterpolant object is actually using the orbital derivative linear functional. If the
# vector field is just equal to 1, that is the same as the function derivative linear functional.


def f(_): return np.array([1])

# Pass in the Data points in the correct order - orbderivs first, then function values. If only orbital
# derivatives are being specified, pass in an empty second Data object
inter_orbderiv = OrbDerivInterpolant([testpts_derivs, testpts], K, f)
inter_orbderiv.solve_linear_system()


def s_orbderiv(x): return inter_orbderiv.eval(x)

for i in range(testpts.points.shape[0]):  # Number of data points
    print('Function value at {} = {}'.format(testpts.points[i, :], s_orbderiv(testpts.points[i, :])))

plt.figure()
xrange = np.array(np.linspace(-1, 5, 100))
xrange_input = xrange.reshape(100, 1)

plt.plot(xrange, s_orbderiv(xrange_input))
plt.title("Interpolant")
plt.show()

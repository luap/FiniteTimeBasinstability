import scipy.spatial.distance as sp
from rtree.index import Rtree, Property # install via conda install --channel https://conda.anaconda.org/IOOS rtree
from pik_parallel_tools.batch_framework import *


apply_filter = np.vectorize(lambda x, f: 1. if x > f else 0.)

nan_filter = np.vectorize(lambda x: 1. if np.isfinite(x) else 0.)

regularized_norm = np.vectorize(lambda x: 1./x if not x == 0. else 1.)

regularized_inv = np.vectorize(lambda x: 1./x if not x == 0. else 0.)


def test_convergence(fmax, times, abs_tol=1e-1, inc_crit=0., ts_frac=0.5, talk=0):
    from scipy.stats import linregress

    L = len(times)
    slope, intercept, r_value, p_value, std_err = linregress(times[int(ts_frac * L):], np.log(fmax)[int(ts_frac * L):])
    if talk:
        print r"slope {:3f}, intercept {:3f}, r_val {:3f}, p_val {:3f},std_err {:3f}".format(slope, intercept, r_value,
                                                                                             p_value, std_err)
    if slope <= inc_crit and fmax[-1] < abs_tol:
        return True
    else:
        return False


# def indicator_function(rhs, point, tol=1e-6):
#     future = np.linspace(0, 200, 2000)
#     projection = odeint(rhs, point, future)
#     return test_convergence(np.abs(projection)[:, 1], future, abs_tol=tol, ts_frac=0.75)


class BatchSwing(BatchRunParameters):
    """" The basic parameters of the simulation:
	How many batches will we have, and how many runs will we do per batch. """

    def __init__(self, number_of_batches=1, simulations_per_batch=1, filt=[0.001, 0.01, 0.1, 1.], nT=1, step=0.01, Tmin=1, Tmax=2):
        super(BatchSwing, self).__init__(number_of_batches=number_of_batches, simulations_per_batch=simulations_per_batch)
        self.deltaT = np.linspace(Tmin, Tmax, number_of_batches, endpoint=True, dtype=np.int)
        #self.deltaT = np.array(np.logspace(np.log10(Tmin), np.log10(Tmax / 10.), number_of_batches, base=10) * 10., dtype=np.int)
        self.nT = nT
        self.step = step
        self.filt = filt


class SwingStochastic(RCGenRand):
    def __init__(self, brp, rhs_par, max_rand, rho="uniform"):
        assert isinstance(brp, BatchSwing)
        assert isinstance(rhs_par, SwingPars)
        super(SwingStochastic, self).__init__(brp, rhs_par, max_rand=max_rand)
        self.nT = brp.nT
        self.rho = rho

    def single(self, batch, run):
        if self.rho == "gauss":
            return self.max_rand * np.random.normal(0, 1, self.system_dimension)
        elif self.rho == "uniform":
            return self.max_rand * (1. - 2. * np.random.random(self.system_dimension))
        else:
            raise ValueError()

    def gen(self, batch, run):
        return (batch, [self.single(batch, run) for i in range(self.nT)])


class SwingPars(RhsParameters):
    def __init__(self, sytem_size=1):
        super(SwingPars, self).__init__(system_size=sytem_size)
        self.system_size = sytem_size
        self.system_dimension = 2 * self.system_size
        self.a = 0.1 * np.ones(self.system_size)  # 0.1
        self.p = np.ones(self.system_size)  # 1
        self.k = 8. * np.ones(self.system_size)
        self.fix_point = np.append(np.arcsin(1. * self.p / self.k), 0)
        if self.system_size > 1:
            self.p[::2] *= -1.
            np.random.shuffle(self.p)
            self.k = np.diag(8. * np.ones(self.system_size), 1) + np.diag(8. * np.ones(self.system_size), -1)  # 8
            self.fix_point = np.nan


class SwingGen(RhsGen):
    def __init__(self, pars, brp, rc_func, tree):
        super(SwingGen, self).__init__(pars)
        assert isinstance(pars, SwingPars)
        assert isinstance(brp, BatchSwing)
        self.step = brp.step
        self.nT = brp.nT
        self.deltaT = brp.deltaT
        self.fix_point = pars.fix_point
        self._rhs = self.gen_Swing_rhs(pars)
        self.system_dimension = pars.system_dimension
        self.rc_func = rc_func
        self.tree = tree

    def gen_Swing_rhs(self, pars):
        assert isinstance(pars, SwingPars)

        def rhs_network(y, t):
            phases = np.exp(1.j * y[:pars.system_size])
            # watch!! I needed to change imag to real part of power flow (see statistic rhs below)
            return np.append(y[pars.system_size:], pars.p - pars.a * y[pars.system_size:] - np.real(phases * pars.k.dot(phases).conjugate()))

        def rhs_single(y, t):
            return np.append(y[1], pars.p - pars.a * y[1] - pars.k * np.sin(y[0]))

        if pars.system_size == 1:
            return rhs_single
        else:
            return rhs_network

    def indicator_function(self, state):
        n = list(self.tree.nearest((state[0], state[1], state[0], state[1]), objects=True))[0]
        return n.object

    def create_ts(self, rc, _):
        batch, _ = rc
        times = np.linspace(0, self.deltaT[batch], int(self.deltaT[batch] / self.step))

        counter = 0
        new_state = self.fix_point # start in fixed point
        timeseries = np.copy(self.fix_point)

        ind = self.indicator_function
        while ind(new_state):
            res = odeint(self._rhs, new_state, times)
            # normalise phase variable
            # otherwise, the indicator function might fail detecting the nearest point?!
            res[:, 0] = np.mod(res[:, 0] + np.pi, 2 * np.pi) - np.pi
            if counter == 0:
                timeseries = np.copy(res)
            else:
                timeseries = np.vstack((timeseries, np.copy(res)))
            new_state = res[-1, :] + self.rc_func(batch, 0)
            counter += 1

        return timeseries


class SwingObs(Observer):
    def __init__(self, brp, par, tree, batch_dir):
        assert isinstance(brp, BatchSwing)
        assert isinstance(par, SwingPars)
        super(SwingObs, self).__init__(brp, par, batch_dir)
        self.nT = brp.nT
        self.deltaT = brp.deltaT
        self.filt = brp.filt
        self.step = brp.step

        self.escape_time = np.zeros(brp.simulations_per_batch)
        self.number_of_jumps = np.zeros_like(self.escape_time)
        self.random_jumps = np.zeros((brp.simulations_per_batch, brp.nT, par.system_dimension))

    def update(self, timeseries, run, initial_conditions=None):
        batch, ic = initial_conditions
        self.random_jumps[run, :, :] = np.array(ic)
        self.escape_time[run] = timeseries.shape[0] * self.step
        self.number_of_jumps[run] = self.escape_time[run] / float(self.deltaT[batch])

def escape_rate():
    np.random.random(42)

    @run_on_master
    def prep_dir(sim_dir):
        result_file = os.path.join("simulation_data", "results_escape_rate.hdf")
        batch_dir = os.path.join(sim_dir, "batch_data")
        if not os.path.exists(batch_dir):
            os.makedirs(batch_dir)
        return result_file, batch_dir

    res = prep_dir("simulation_data")
    res = comm.bcast(res, root=0)
    result_file, batch_dir = res

    tree = Rtree(os.path.join("simulation_data", "rtree"), interleaved=True)

    brp = BatchSwing(number_of_batches=10, simulations_per_batch=1000, step=0.1, filt=1e-6, Tmin=1, Tmax=400)
    par = SwingPars()
    rcg = SwingStochastic(brp, par, max_rand=[np.pi/3., 5.], rho="uniform")
    gen = SwingGen(par, brp, rcg.single, tree)
    obs = SwingObs(brp, par, tree, batch_dir)

    run_simulation(result_file, brp, gen, rcg, obs, 0, mpi_enabled=True)

if __name__ == "__main__":
    escape_rate()

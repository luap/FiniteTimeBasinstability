# export MKL_NUM_THREADS=1
# export NUMEXPR_NUM_THREADS=1
# export OMP_NUM_THREADS=1

from __future__ import division, print_function, absolute_import, unicode_literals

import scipy.sparse

import numpy as np
import baobap as bao
from numba import njit
import os, sys
import copy

default_dir = os.path.join("simulation_data", "swing_with_alpha")



class SwingParameters():
    """
        Data type for the simulation parameters. This is the data passed to define the right hand side of the ODE.
    """

    def __init__(self, system_size=1):
        """
        :param system_size: the size of the system
        :return:
        """
        self.system_size        = system_size
        self.system_dimension   = 2 * system_size
        self.phase_coupling     = np.zeros((system_size, system_size), dtype=np.float64)
        self.damping_coupling   = np.zeros(system_size, dtype=np.float64)
        self.input_power        = np.zeros(system_size, dtype=np.float64)
        self.fix_point          = np.zeros(self.system_dimension, dtype=np.float64)


def gen_swing_rhs(swing_parameters):
    """
    This function defines a rhs function with the data given by simulation_run using sparse matrix multiplication.
    :param swing_parameters: an instance of SimulationRun that defines the system parameters.
    :return: A function of signature rhs(y, t)
    """
    assert isinstance(swing_parameters, SwingParameters)

    size_of_system = int(swing_parameters.system_size)
    input_power = np.array(swing_parameters.input_power, dtype=np.float64)
    damping_coupling = np.array(swing_parameters.damping_coupling, dtype=np.float64)
    phase_coupling_sp = scipy.sparse.csr_matrix(swing_parameters.phase_coupling, dtype=np.complex128)

    # We start with phase coupling purely positive imaginary, and then rotate it towards a positive real component by
    # multiplying e^{-i \alpha} on.

    def right_hand_side_sparse(y, _, e_mi_alpha):
        phases = np.exp(1.j * y[:size_of_system])
        return np.append(y[size_of_system:], input_power - damping_coupling * y[size_of_system:] -
                         np.real(phases * phase_coupling_sp.dot(e_mi_alpha * phases).conjugate()))

    return right_hand_side_sparse


def load_from_json(filename, shuffle_power=False, seed=None, coupling_factor = 1.):
    import json

    with open(filename) as f:
        data = json.load(f)

    NN = len(data["nodes"])

    P = np.empty(NN)
    damping = np.empty(NN)

    for n in data["nodes"]:
        P[n["id"]] = n["input_power"]
        damping[n["id"]] = n["damping"]

    if shuffle_power:
        if seed is not None:
            np.random.seed(seed)
        np.random.shuffle(P)

    Y = scipy.sparse.lil_matrix((NN,NN), dtype=np.complex128)

    # The json file needs to contain "links" with "source" and "target" and a
    # "coupling" field.

    for e in data["links"]:
        Y[e["source"], e["target"]] = coupling_factor * e["coupling"] * 1.j
        Y[e["target"], e["source"]] = coupling_factor * e["coupling"] * 1.j

    diagonal_sum = Y.dot(np.ones(NN))

    for i in range(NN):
        Y[i,i] = - diagonal_sum[i]

    spars = SwingParameters(NN)

    spars.phase_coupling = scipy.sparse.csr_matrix(Y)
    spars.damping_coupling = damping
    spars.input_power = P

    return spars


@bao.run_on_master
def get_generalized_fixpoint(swingpar, guess=None):
    assert isinstance(swingpar, SwingParameters)
    def rootfunc(y):
        w_global = y[0]
        phase = np.exp(1.j * y)
        phase[0] = 1.
        flow = np.conj(swingpar.phase_coupling.dot(phase))
        return swingpar.input_power - swingpar.damping_coupling * w_global \
               - np.real(phase * swingpar.phase_coupling.dot(phase).conjugate())

    g2 = np.zeros(swingpar.system_size)

    if guess is not None:
        g2[1:] = guess[1:swingpar.system_size] - guess[0]
        g2[0] = np.average(guess[swingpar.system_size:])

    from scipy.optimize import root

    fixpoint = np.zeros(swingpar.system_dimension)

    res = root(rootfunc, g2, method='Krylov', tol=1e-07)

    if res.success:
        fixpoint[1:swingpar.system_size] = res.x[1:]
        fixpoint[swingpar.system_size:] = res.x[0]
        return fixpoint
    else:
        return None


def def_rc_gen_square(swingpar, n_phase, n_freq, fps, alphas, max_freq=10., max_phase=np.pi, node=0):
    assert isinstance(swingpar, SwingParameters)

    n = node

    def generate_run_conditions(batch, run):
        # The node to disturb:

        ic = np.copy(fps[run])

        # Phase and frequency perturbation, with variance given as above:
        ic[n] = max_phase - (batch % n_phase) / (n_phase - 1) * 2. * max_phase
        ic[n + swingpar.system_size] = max_freq - np.floor(batch / n_phase) / (n_freq - 1) * 2. * max_freq

        e_alpha = np.exp(1.j * alphas[run])

        return ic, (e_alpha,)

    return generate_run_conditions


@bao.run_on_master
def find_fps(swingpar, alphas, max_alpha=0.2, slowdown=100., post_bif_integrate=0):
    import numbers
    assert isinstance(alphas, np.ndarray) or isinstance(alphas, numbers.Integral)
    if type(alphas) is np.ndarray:
        n_alpha = len(alphas)
    else:
        n_alpha = int(alphas)
        alphas = np.linspace(0., max_alpha, n_alpha)

    fps = np.zeros((n_alpha, swingpar.system_dimension))

    print("Looking for base case")
    res = get_generalized_fixpoint(swingpar)

    if res is not None:
        fps[0] = res
    else:
        print("No fixed point found at " + str(alphas[0]))
        bao.comm.Abort()

    from scipy.integrate import odeint

    rhs = gen_swing_rhs(swingpar)

    rhs_bif = lambda y, t: rhs(y, 0., np.exp(1.j * t / slowdown))

    try:
        alpha_min = np.min(np.abs(alphas[1:] - alphas[:-1]))
    except:
        alpha_min = max_alpha

    states = odeint(rhs_bif, fps[0], alphas * slowdown, mxstep=100 * int(slowdown))

    # print("Bifurcation run done.")

    states[:, :swingpar.system_size] = states[:, :swingpar.system_size] - states[:, 0, np.newaxis]

    for i in range(1, n_alpha):
        print("Looking for case {} of {}".format(1+i, n_alpha))
        swingpar_alpha = copy.deepcopy(swingpar)
        swingpar_alpha.phase_coupling *= np.exp(1.j * alphas[i])
        if not post_bif_integrate == 0:
            con_states = odeint(rhs,  states[i], np.linspace(0, post_bif_integrate, int(post_bif_integrate)),
                                args=(np.exp(1.j * alphas[i]), ), mxstep=int(1e9), hmax=alpha_min/slowdown)
            ic = con_states[-1]
        else:
            ic = states[i]
        res = get_generalized_fixpoint(swingpar_alpha, ic)

        if res is not None:
            print("Fixed point found for " + str(alphas[i]))
            fps[i] = res
        else:
            print("No fixed point found at " + str(alphas[i]))
            return None


    return fps, alphas


def def_rc_gen(swingpar, fps, alphas, freq_var=10., phase_var=np.pi/2):
    assert isinstance(swingpar, SwingParameters)


    def generate_run_conditions(batch, run):
        # The node to disturb:

        ic = np.copy(fps[run])

        n = np.random.randint(swingpar.system_size)

        # Phase and frequency perturbation, with variance given as above:
        ic[n] = np.random.randn() * phase_var
        ic[n + swingpar.system_size] = np.random.randn() * freq_var

        e_alpha = np.exp(1.j * alphas[run])

        return ic, (e_alpha,)

    return generate_run_conditions

def def_rc_gen_global(swingpar, fps, alphas, freq_var=10., phase_var=np.pi/2):
    assert isinstance(swingpar, SwingParameters)


    def generate_run_conditions(batch, run):
        # The node to disturb:

        ic = np.copy(fps[run])

        #n = np.random.randint(swingpar.system_size)

        # Phase and frequency perturbation, with variance given as above:
        ic[:swingpar.system_size] += np.random.normal(0, 1, swingpar.system_size) * phase_var
        ic[swingpar.system_size:] += np.random.normal(0, 1, swingpar.system_size) * freq_var

        e_alpha = np.exp(1.j * alphas[run])

        return ic, (e_alpha,)

    return generate_run_conditions


# This function observes the results of the simulation run and returns its observations in a dict
# noinspection PyUnusedLocal
def def_swing_ob(swingpar, times):
    n_times = len(times) - len(times) // 10
    frequ_id = range(swingpar.system_size, 2*swingpar.system_size)

    def swing_ob(time_series, rc):
        return{"asymptotic_frequencies": np.average(time_series[n_times:, frequ_id], axis=0)}
    return swing_ob

def def_ftbs_ob(swingpar, times, threshold=None, freq_var=10., phase_var=np.pi/2):
    frequ_id = range(swingpar.system_size, 2*swingpar.system_size)

    R = np.append(np.tile(phase_var, swingpar.system_size), np.tile(freq_var, swingpar.system_size))


    stepfunc = np.vectorize(lambda x, f: 0. if x < f else 1.)

    prod = lambda a, b: a * b

    if threshold is None:
        threshold = np.logspace(-6, -1, 6)

    def iota_uniform(point=[0, 0], rel=True):
        # px, py = np.abs(point)
        # a, b = R
        if rel:
            point = np.array(point) - swingpar.fix_point

        overlap = reduce(prod, stepfunc(2. * R - np.array(point), 0))

        if overlap == 0:
            return 2.
        else:
            # V grows exponentially with the system size
            V = np.sum(np.log10(2.*R))
            return 2. - 2. * np.exp(np.log10(overlap) - V)
        # return 2. - 2. * max(0, 2. * a - point[0]) * max(0, 2. * b - point[1]) / V

    def find_idx(vals, e, first=False, less=False, nothing=[]):
        if less:
            idx = np.where(np.array(vals) < e)[0]
        else:
            idx = np.where(np.array(vals) > e)[0]
        # print idx
        if len(idx) is 0:
            return nothing
        else:
            if first:
                return idx[0]
            else:
                return idx[-1]

    def ftbs_ob(time_series, rc):
        val = [iota_uniform(p) for p in time_series]
        return{"epsilon": threshold, "return time": np.array([find_idx(val, t, nothing=np.nan) for t in threshold])}
    return ftbs_ob

@bao.run_on_master
def plot_fps(sim_dir, alphas, fps, swingpar):
    import matplotlib
    matplotlib.use("Agg")
    import matplotlib.pyplot as plt

    plt.figure()
    plt.title("Frequencies of the sync state")
    plt.xlabel("Phase shift")
    plt.plot(alphas, fps[:, swingpar.system_size:])
    plt.savefig(os.path.join(sim_dir, "sync_frequency.png"))

    phase_change = np.abs(fps[-1, :swingpar.system_size] - fps[0, :swingpar.system_size])

    rgba_colors = np.zeros((swingpar.system_size, 4))
    rgba_colors[:, 3] = 0.01 + 0.6 * phase_change / np.max(phase_change)

    plt.figure()
    plt.title("Phases of the sync state\n(strongly changing phases highlighted)")
    plt.xlabel("Phase shift")
    for fp, c in zip(fps.T[:swingpar.system_size], rgba_colors):
        plt.plot(alphas, fp, color=c)
    plt.savefig(os.path.join(sim_dir, "sync_phases.png"))

    plt.clf()


@bao.run_on_master
def analyse(result_file):
    res_dir = os.path.dirname(result_file)

    try:
        cluster_size = np.load(os.path.join(res_dir, "cluster_size_mean.npy"))
        n_clusters = np.load(os.path.join(res_dir, "ncluster.npy"))
        number_of_desync = np.load(os.path.join(res_dir, "number_of_desync.npy"))

    except:
        print("read in hdf -> takes some time")
        res = bao.load_all_fields_from_results(result_file)
        # print(res.keys())

        af = res["asymptotic_frequencies"]
        batches, runs, sys_size = af.shape

        number_of_desync = np.zeros((batches, runs))
        n_clusters = np.zeros((batches, runs))
        cluster_size = np.zeros((batches, runs, sys_size))

        for b in range(batches):
            for r in range(runs):
                #number_of_desync[b, r] = detect_solitary(af[b, r])
                n_clusters[b, r], cs, me = detect_clusters(af[b, r])
                cluster_size[b, r, np.array(cs)-1] = me
                number_of_desync[b, r] = int(sys_size - np.max(cs)) / sys_size
                # print("batch {}, run {}: {}".format(b,r,d))

        np.save(os.path.join(res_dir, "cluster_size_mean"), cluster_size)
        np.save(os.path.join(res_dir, "number_of_desync"), number_of_desync)
        np.save(os.path.join(res_dir, "ncluster"), n_clusters)

    average_number_of_desync = np.mean(number_of_desync, axis=0) *1.
    fraction_of_solitaries = np.mean(number_of_desync == 1, axis=0)
    bs = np.mean(number_of_desync == 0, axis=0)

    _, args = bao.load_state_for_analysis(os.path.join(os.path.dirname(result_file), "analysis.p"))

    sim_dir, alphas, fps, swingpar = args

    import matplotlib
    matplotlib.use("Agg")
    import awesomeplot.core as ap

    # initialise plotting canvas
    canvas = ap.Plot(output="paper")
    fnames = []

    fig_combine = canvas.add_lineplot(x=alphas, lines={"sync": bs, "sol":fraction_of_solitaries}, #"desync":average_number_of_desync},
                                 labels=[r"phase lag $\alpha$", ""], legend=True, linewidth=2, marker="o", infer_layout=False)
    fnames.append(os.path.join(sim_dir, "combined"))

    mce = np.mean(cluster_size, axis=0)
    n = int(cluster_size.shape[2] / 2.)
    cslist = [0, 1, 2, 3, 4, n-2, n-1, n]

    fig_mce = canvas.add_lineplot(x=alphas, lines={k+1: mce[:, k] for k in cslist},
                                      # "desync":average_number_of_desync},
                                      labels=[r"phase lag $\alpha$", ""], legend=True, linewidth=2, marker="o",
                                      infer_layout=False)
    canvas.set_log(fig_mce, "y")
    fnames.append(os.path.join(sim_dir, "mean_clust_freq"))

    # fig_bs = canvas.add_lineplot(x=alphas, lines={0:bs}, labels=[r"phase lag $\alpha$", r"average single-node basin stability"], legend=False)
    # fnames.append(os.path.join(sim_dir, "basin_stability"))
    #
    # fig_desync = canvas.add_lineplot(x=alphas, lines={0: average_number_of_desync}, labels=[r"phase lag  $\alpha$", "average # desync oscillators"],
    #                             legend=False)
    # fnames.append(os.path.join(sim_dir, "average_desync"))
    #
    # fig_desync = canvas.add_lineplot(x=alphas, lines={0: fraction_of_solitaries}, labels=[r"phase lag  $\alpha$", "fraction of solitary-1 states"],
    #                                  legend=False)
    # fnames.append(os.path.join(sim_dir, "frac_sol"))

    fig_n = canvas.add_lineplot(x=alphas, lines={"mean": np.mean(n_clusters, axis=0), "max": np.max(n_clusters, axis=0)},
                                 labels=[r"phase lag $\alpha$", r"# frequency clusters"], legend=True)
    fnames.append(os.path.join(sim_dir, "n_clust"))

    canvas.save(fnames)

    print(average_number_of_desync)
    print(bs)


@bao.run_on_master
def analyse_picture(result_file):
    res = bao.load_all_fields_from_results(result_file)
    # print(res.keys())
    _, args = bao.load_state_for_analysis(os.path.join(os.path.dirname(result_file), "analysis.p"))

    sim_dir, alphas, fps, swingpar, node, sides = args

    af = res["asymptotic_frequencies"]
    batches, runs, sys_size = af.shape

    number_of_desync = np.zeros((batches, runs))

    for b in range(batches):
        for r in range(runs):
            number_of_desync[b, r] = detect_solitary(af[b, r])
            # print("batch {}, run {}: {}".format(b,r,d))

    import matplotlib as mpl
    mpl.use("Agg")
    import matplotlib.pyplot as plt
    cmap = mpl.colors.ListedColormap(['black', 'blue', 'red'])
    bounds = [0, 1, 2, 3]
    norm = mpl.colors.BoundaryNorm(bounds, cmap.N)

    for i, a in enumerate(alphas):
        plt.figure()

        plt.imshow(number_of_desync[:, i].reshape((sides, sides)), interpolation='nearest',
                            cmap=cmap, norm=norm)

        plt.title("Number of desync oscillators\n Perturbation at node {} for phase shift {}".format(node, a))
        plt.colorbar(cmap=cmap, norm=norm, boundaries=bounds, ticks=[0, 1, 2])
        plt.savefig(os.path.join(sim_dir, "phase_portrait_{}.png".format(i)))


def detect_solitary(freq):
    # How many points are outside the largest cluster of asymptotic frequencies:
    from sklearn.cluster import DBSCAN

    db = DBSCAN(eps=0.3, min_samples=1).fit(freq[:, np.newaxis])

    labels = db.labels_

    s_labels = set(labels)

    n_clusters = len(set(labels))

    size_of_cluster = np.zeros(n_clusters)

    for i, l in enumerate(s_labels):
        size_of_cluster[i] = np.count_nonzero(labels == l)

    return int(len(freq) - np.max(size_of_cluster))

def detect_clusters(freq):
    from sklearn.cluster import DBSCAN
    from sklearn import metrics

    if len(freq.shape) == 1:
        X = freq[:, np.newaxis]
    else:
        X = freq

    db = DBSCAN(min_samples=1).fit(X)
    core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
    core_samples_mask[db.core_sample_indices_] = True
    labels = db.labels_

    # Number of clusters in labels, ignoring noise if present.
    unique_labels, cluster_size = np.unique(labels, return_counts=True)
    n_clusters_ = len(unique_labels)
    means = np.zeros(n_clusters_)
    for i, k in enumerate(unique_labels):
        means[i] = np.nanmean(X[labels == k, 0])

    return n_clusters_, cluster_size, means

@bao.run_on_master
def find_sim_dir(base_dir, base_str):
    cnum = 0
    while os.path.exists(os.path.join(base_dir, base_str + str(cnum))):
        cnum += 1
    sim_dir = os.path.join(base_dir, base_str + str(cnum))
    return sim_dir

@bao.run_on_master
def write_swingpars_to_resfile(result_file, swingpar):
    import h5py
    with h5py.File(result_file) as h5f:
        swing_for_save = copy.deepcopy(swingpar)
        swing_for_save.phase_coupling = swingpar.phase_coupling.todense()
        h5f.create_group("Swing_Parameters")
        bao.save_class_to_hdf5_group(swing_for_save, h5f["Swing_Parameters"])
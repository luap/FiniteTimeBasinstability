"""
For local testing with MPI remember to disable the internal threading:
export MKL_NUM_THREADS=1
export NUMEXPR_NUM_THREADS=1
export OMP_NUM_THREADS=1
"""


from __future__ import division, print_function, unicode_literals

import os
import sys
import copy

from swing_with_alpha import SwingParameters, find_fps

import scipy

import baobap as bao
import numpy as np
import cPickle as pickle
default_dir = os.path.join("simulation_data", "swing_with_alpha")


def define_circle_pars(n, p, Omega, K, damping):
    P = np.empty(n)
    damp = np.empty(n)

    for i in range(n):
        P[i] = (1. - 2. * (i % 2)) * Omega
        damp[i] = damping


    Y = scipy.sparse.lil_matrix((n,n), dtype=np.complex128)

    # The json file needs to contain "links" with "source" and "target" and a
    # "coupling" field.

    for i in range(n):
        for j in range(-p,p+1):
            if not j == 0:
                Y[i, (i+j) % n] = K * 1.j
                Y[(i+j) % n, i] = K * 1.j

    diagonal_sum = Y.dot(np.ones(n))

    for i in range(n):
        Y[i,i] = - diagonal_sum[i]

    spars = SwingParameters(n)

    spars.phase_coupling = scipy.sparse.csr_matrix(Y)
    spars.damping_coupling = damp
    spars.input_power = P

    return spars


def setup_circle(n_alphas=20, N=50, P=2, Omega=0.1, mu=4., epsilon=0.1):
    swingpar = define_circle_pars(N, P, Omega, mu/(2*P), epsilon)
    fps, alphas = find_fps(swingpar, n_alphas, slowdown=100., post_bif_integrate=0)
    circle_pars = {'N':N, 'P':P, 'Omega':Omega, 'mu':mu, 'epsilon':epsilon}

    return swingpar, fps, alphas, circle_pars

def load_setup(name, base_dir=default_dir):
    bao.comm.Barrier()
    with open(os.path.join(base_dir, "setups", name), mode='r') as f:
        setup = pickle.load(f)
    return setup

@bao.run_on_master
def save_setup(name, setup, base_dir=default_dir):
    if not os.path.exists(os.path.join(base_dir, "setups")):
        os.makedirs(os.path.join(base_dir, "setups"))

    with open(os.path.join(base_dir, "setups", name), mode='w') as f:
        pickle.dump(setup, f)


@bao.run_on_master
def create_setup(base_dir, name, swingpar, n_alphas):
    print("Creating setup - rank {}".format(bao.rank))
    res = find_fps(swingpar, n_alphas, post_bif_integrate=0)
    if res is None:
        print("Fix point search failed")
        bao.comm.Abort()
    fps, alphas = res
    print("Saving setup - rank {}".format(bao.rank))
    save_setup(name, (swingpar, fps, alphas), base_dir=base_dir)

import numpy as np
import matplotlib.pyplot as plt

from skimage import measure



def area(vs):
    a = 0
    x0,y0 = vs[0]
    for [x1, y1] in vs[1:]:
        dx = x1 - x0
        dy = y1 - y0
        a += 0.5 * (y0 * dx - x0 * dy)
        x0 = x1
        y0 = y1
    return a

# Generate some test data.
delta = 0.01
x = np.linspace(-np.pi, np.pi, 100)
y = np.linspace(-np.pi, np.pi, 100)
X, Y = np.meshgrid(x, y)
r = np.sqrt(X**2 + Y**2)

# # Find contours at a constant value
# plt.figure()
# contours = measure.find_contours(r, 1)
# for n, contour in enumerate(contours):
#     print n
#     plt.plot(contour[:, 0], contour[:, 1], linewidth=2)
# print area(contour)
#
# verts, faces = measure.marching_cubes(r, 1.0, (1., 1., 1.))

plt.figure()
levels = [1., 2., 3.]
cs = plt.contour(X, Y, r,levels=levels)

for i in range(len(levels)):
    contour = cs.collections[i]
    vs = contour.get_paths()[0].vertices
    # Compute area enclosed by vertices.
    a = area(vs)
    print("r = " + str(levels[i]),  " a =" + str(a))


plt.show()
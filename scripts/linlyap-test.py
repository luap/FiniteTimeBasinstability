import numpy as np
import scipy.linalg as la

def info(M, name='...'):
    print "The matrix " + name + ":"
    print M
    print "\nThe eigenvalues of " + name + ":"
    print la.eig(M)[0]
    print ""

A = np.diag([-0.01,-0.02,-0.3])


A[0,1] = 2.
A[1,0] = -0.5

info(A,'A')
info(A.T + A, "A.T + A")

def lyap1(A):
    Q = -1. * la.sqrtm(A.T.dot(A))
    info(Q,'Q')
    return la.solve_lyapunov(A, Q)


def lyap2(A):
    A_sym = A.T + A
    Q = -1. * la.sqrtm(A_sym.dot(A_sym))
    info(Q,'Q')
    return la.solve_lyapunov(A, Q)


def lyap3(A):
    # This would work IF A.T + A were negative semidefinite which it's generally not. If it's not, then
    Q = A.T + A
    info(Q,'Q')
    return la.solve_lyapunov(A, Q)

def is_lyap(A,L):
    
    Q = L.dot(A.T) + A.dot(L)


def decomp(A):
    w, vl, vr = la.eig(A, left=True, right=True)
    print w
    D = np.diag(w)

    # We can use the matrix of right eigenvectors to diagonalize the matrix A:
    print vr.dot(D.dot(la.inv(vr))) - A

    # The left eigenvectors are orthogonal to the (complex conjugate) right eigenvectors, except where they refer to the
    # same eigenvalue:
    print vl.conjugate().T.dot(vr)

    # The juggling around with complex conjugate eigenvalues is necessary due to scipe.linal.eig conventions
    # We can normalize them such that the inner product between right eigenvector and left eigenvector is 0 or 1:
    vl_h = (vl.conjugate() / np.diag(vl.conjugate().T.dot(vr))).T

    # Now vl_h is the inverse of vr and we can use it to diagonalize A without using an inverse:
    print vr.dot(D.dot(vl_h)) - A

    # What is the meaning here: the left eigenvector for an eigenvalue lambda is orthogonal to all right eigenvectors to
    # other eigenvalues. Thus projecting a vector onto the left eigenvector gives exactly the part of it that can not
    # be expressed other than through the right eigenvector associated to lambda.


decomp(A)

exit()

info(lyap1(A),'L1')
 
info(lyap2(A),'L2')

info(lyap3(A), 'L3')
#!/bin/bash
#SBATCH --qos=short
#SBATCH --job-name=ftbsnet
#SBATCH --output=ftbsnet.out
#SBATCH --error=ftbsnet.err
#SBATCH --tasks=200

export I_MPI_PMI_LIBRARY=/p/system/slurm/lib/libpmi.so
module load anaconda/4.2.0
module load intel/2017.4
export PYTHONPATH=$PYTHONPATH:"/data/condynet/condypackages/lib/python2.7/site-packages/"

export MKL_NUM_THREADS=1
export NUMEXPR_NUM_THREADS=1
export OMP_NUM_THREADS=1

##################
echo "------------------------------------------------------------"
echo "SLURM JOB ID: $SLURM_JOBID"
echo "$SLURM_NTASKS tasks"
echo "------------------------------------------------------------"

srun -n 200 python libftbs.py


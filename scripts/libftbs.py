import scipy.spatial.distance as sp
try:
    from rtree.index import Rtree, Property # install via conda install --channel https://conda.anaconda.org/IOOS rtree
except:
    print "cannot load rtree"

from pik_parallel_tools.batch_framework import *

apply_filter = np.vectorize(lambda x, f: 1. if x > f else 0.)

nan_filter = np.vectorize(lambda x: 1. if np.isfinite(x) else 0.)

regularized_norm = np.vectorize(lambda x: 1./x if not x == 0. else 1.)

regularized_inv = np.vectorize(lambda x: 1./x if not x == 0. else 0.)

prod = lambda a, b: a * b

stepfunc = np.vectorize(lambda x, f: 0. if x < f else 1.)

class BatchSwing(BatchRunParameters):
    """" The basic parameters of the simulation:
    How many batches will we have, and how many runs will we do per batch. """

    def __init__(self, number_of_batches=1, simulations_per_batch=1, filt=[0.001, 0.01, 0.1, 1.], time=10., step=0.01):
        super(BatchSwing, self).__init__(number_of_batches=number_of_batches, simulations_per_batch=simulations_per_batch)


        self.times = np.arange(0, time, step)

        self.step = step
        self.filt = filt

class SwingPars(RhsParameters):
    def __init__(self, sytem_size=1):
        super(SwingPars, self).__init__(system_size=sytem_size)
        self.system_size = sytem_size
        self.system_dimension = 2 * self.system_size
        self.a = 0.1 * np.ones(self.system_size)  # 0.1
        self.p = np.ones(self.system_size) # 1
        self.k = 8. * np.ones(self.system_size)
        self.fix_point = np.append(np.arcsin(1. * self.p / self.k), 0)
        if self.system_size > 1:
            self.p[::2] *= -1.
            np.random.shuffle(self.p)
            self.k = np.diag(8. * np.ones(self.system_size), 1) + np.diag(8. * np.ones(self.system_size), -1) # 8
            self.fix_point = np.nan


class SwingGen(RhsGen):
    """ The time series generator for the Lorenz system. """
    def __init__(self, pars):
        assert isinstance(pars, SwingPars)
        super(SwingGen, self).__init__(pars)
        self._rhs = self.gen_Swing_rhs(pars)

    def gen_Swing_rhs(self, pars):
        assert isinstance(pars, SwingPars)
        fpp = pars.fix_point[:pars.system_size]

        def rhs_network(y, t):
            # shift phases such that fp is at origin
            phases = np.exp(1.j * (y[:pars.system_size] + fpp))
            # watch!! I needed to change imag to real part of power flow (see statistic rhs below)
            return np.append(y[pars.system_size:], pars.input_power - pars.damping_coupling * y[pars.system_size:] - np.real(phases * pars.k.dot(
                phases).conjugate()))

        def rhs_single(y, t):
            return np.append(y[1], pars.p - pars.a * y[1] - pars.k * np.sin(y[0]))

        if pars.system_size == 1:
            return rhs_single
        else:
            return rhs_network

class SwingObsNetwork(Observer):

    def __init__(self, brp, par, batch_dir,freq_var=10., phase_var=np.pi/2):
        assert isinstance(brp, BatchSwing)
        assert isinstance(par, SwingPars)

        super(SwingObsNetwork, self).__init__(brp, par, batch_dir)

        self.filt = brp.filt
        self.integration_step = brp.step
        self.fix_point = par.fix_point
        self.system_size = par.system_size

        self.R = np.append(np.tile(phase_var, par.system_size), np.tile(freq_var, par.system_size))

        self.max_abs_values = np.zeros((brp.simulations_per_batch, par.system_size))
        self.fin_abs_values = np.zeros((brp.simulations_per_batch, par.system_dimension))
        self.return_time = np.zeros((brp.simulations_per_batch, len(brp.filt)))
        self.conv = np.zeros(brp.simulations_per_batch, dtype=bool)

    def iota_uniform(self, point=[0, 0], fp=None):
        # px, py = np.abs(point)
        # a, b = R
        if fp is not None:
            point = np.array(point) - fp
        overlap = (2 * self.R - np.abs(point)) * stepfunc(2 * self.R - np.abs(point), 0) / (2. * self.R)
        if 0 in overlap:
            # if one overlap is 0, so is the product of all
            return 2.
        else:
            exponent = np.sum(np.log10(overlap))
            return 2. - 2. * 10. ** exponent
        # return 2. - 2. * max(0, 2. * a - point[0]) * max(0, 2. * b - point[1]) / V

    def update(self, timeseries, run, initial_conditions):
        super(SwingObsNetwork, self).update(timeseries, run, initial_conditions)
        self.max_abs_values[run] = np.max(np.abs(timeseries[:, self.system_size:]), axis=0)
        self.fin_abs_values[run] = np.abs(timeseries[-1, :])

        # calculate phases mod 2 pi
        timeseries[:, :self.system_size] = np.mod(timeseries[:, :self.system_size] + np.pi, 2 * np.pi) - np.pi
        # substract the phase of node 0 to ignore global phase shifts to the fixed point at the origin
        nullphase = np.repeat(timeseries[-1, 0], self.system_size)
        timeseries[:, :self.system_size] -= nullphase
        times = np.arange(0, self.integration_step * timeseries.shape[0], self.integration_step)

        self.conv[run] = test_convergence(np.max(np.abs(timeseries[:, self.system_size:]), axis=1), times)

        if self.conv[run]:
            # the data is generated from the relative system
            val = [self.iota_uniform(p) for p in timeseries]
            #val = [self.iota_uniform(p, fp=self.fix_point) for p in timeseries]
            # going backwards, find the last idx which is less then t
            self.return_time[run, :] = self.integration_step* (
                (timeseries.shape[0] - 1) - np.array(
                    [find_idx(val[::-1], t, nothing=np.nan, less=True, first=False) for t in self.filt]
                )
            )
        else:
            self.return_time[run, :] = np.array([times[-1] for t in self.filt])


class SwingObs(Observer):

    def __init__(self, brp, par, batch_dir, Crho):
        assert isinstance(brp, BatchSwing)
        assert isinstance(par, SwingPars)

        super(SwingObs, self).__init__(brp, par, batch_dir)

        self.filt = brp.filt
        self.integration_step = brp.step
        self.fix_point = par.fix_point
        self.system_size = par.system_size

        self.Crho = Crho
        self.set_lin_matrices(par, point=par.fix_point)

        self.max_abs_values = np.zeros((brp.simulations_per_batch, par.system_size))
        self.fin_abs_values = np.zeros((brp.simulations_per_batch, par.system_dimension))
        self.return_time = np.zeros((brp.simulations_per_batch, len(brp.filt)))

    def update(self, timeseries, run, initial_conditions):
        super(SwingObs, self).update(timeseries, run, initial_conditions)
        self.max_abs_values[run] = np.max(np.abs(timeseries[:, self.system_size:]), axis=0)
        self.fin_abs_values[run] = np.abs(timeseries[-1, :])

        timeseries[:, :self.system_size] = np.mod(timeseries[:, :self.system_size] + np.pi, 2 * np.pi) - np.pi

        lprod = np.array([self.lyapunov_prod(timeseries[t], self.fix_point) for t in range(len(timeseries))])
        for i, f in enumerate(self.filt):
            filtered_lprod = apply_filter(lprod, self._lambda_min * (self.Crho * f)**2)
            try:
                self.return_time[run, i] = self.integration_step * (np.where(np.diff(filtered_lprod) != 0)[0][0] + 1)
            except:
                self.return_time[run, i] = self.integration_step * timeseries.shape[0]

    def set_lin_matrices(self, par, point=None):
        # derive matices for linear system
        from scipy.linalg import solve_lyapunov, eigvals
        self._jacobian = np.array([[0., 1.], [- par.k * np.cos(point[0]), - par.a]])
        self._lyapunov = solve_lyapunov(self._jacobian.transpose(), - np.diag(np.ones(2)))
        self._lambda_min = np.real(eigvals(self._lyapunov)).min()

    def lyapunov_prod(self, x, p):
        # x centered around p (i.e. fix point)
        # x[0] = np.mod(x[0], 2 * np.pi)
        return (x - p).dot(self._lyapunov.dot(x - p))

class SwingUniform(RCGenRand):
    def __init__(self, brp, rhs_par, max_rand):
        assert isinstance(brp, BatchSwing)
        assert isinstance(rhs_par, SwingPars)
        super(SwingUniform, self).__init__(brp, rhs_par, max_rand=max_rand)

    def gen_uniform(self, batch, run):
        ic = self.max_rand * (1. - 2. * np.random.random(self.system_dimension))
        return ic

    def gen(self, batch, run):
        return self.gen_uniform(batch, run)

class SwingUniformGlobal(RCGenRand):
    def __init__(self, brp, rhs_par, max_rand):
        assert isinstance(brp, BatchSwing)
        assert isinstance(rhs_par, SwingPars)
        super(SwingUniformGlobal, self).__init__(brp, rhs_par, max_rand=max_rand)
        self.system_size = rhs_par.system_size
        self.fp = rhs_par.fix_point

    def gen_uniform(self, batch, run):
        ic = np.zeros(self.system_dimension) # we are in relative system
        #ic = np.copy(self.fp)
        # Phase and frequency perturbation, with variance given as above:
        ic[:self.system_size] += self.max_rand[0] * (1. - 2. * np.random.random(self.system_size))
        ic[self.system_size:] += self.max_rand[1] * (1. - 2. * np.random.random(self.system_size))
        return ic

    def gen(self, batch, run):
        return self.gen_uniform(batch, run)


class SwingGauss(RCGenRand):
    def __init__(self, brp, rhs_par, max_rand):
        assert isinstance(brp, BatchSwing)
        assert isinstance(rhs_par, SwingPars)
        super(SwingGauss, self).__init__(brp, rhs_par, max_rand=max_rand)

    def gen_gauss(self, batch, run):
        ic = self.max_rand * np.random.normal(0, 1, self.system_dimension)
        return ic

    def gen(self, batch, run):
        return self.gen_gauss(batch, run)

class RCGenGrid(RCGenRand):
    def __init__(self, brp, rhs_par, max_rand=[-1., 1., -1., 1.], **kwargs):
        super(RCGenGrid, self).__init__(brp, rhs_par, max_rand, **kwargs)
        x = np.linspace(max_rand[0], max_rand[1], brp.simulations_per_batch)
        y = np.linspace(max_rand[2], max_rand[3], brp.number_of_batches)
        grid = np.meshgrid(x, y)
        self.xgrid = grid[0]
        self.ygrid = grid[1]

    def gen(self, batch, run):
        return np.array([self.xgrid[batch, run], self.ygrid[batch, run]])

class BasinObs(Observer):

    def __init__(self, brp, par, tree, times, batch_dir):
        super(BasinObs, self).__init__(brp, par, batch_dir)
        self.fix_point = par.fix_point
        self.system_size = par.system_size
        self.times = times

        self.max_abs_values = np.zeros((brp.simulations_per_batch, len(times)))
        self.fin_abs_values = np.zeros((brp.simulations_per_batch, par.system_dimension))
        self.converged = np.zeros(brp.simulations_per_batch)

        self.tree = tree

    def test_convergence(self, fmax, times, abs_tol=1e-1, inc_crit=0., ts_frac=0.75, talk=0):
        from scipy.stats import linregress

        L = len(times)
        slope, intercept, r_value, p_value, std_err = linregress(times[int(ts_frac * L):], np.log(fmax)[int(ts_frac * L):])
        if talk:
            print r"slope {:3f}, intercept {:3f}, r_val {:3f}, p_val {:3f},std_err {:3f}".format(slope, intercept, r_value, p_value, std_err)
        if slope <= inc_crit and fmax[-1] < abs_tol:
            return True
        else:
            return False

    def update(self, timeseries, run, initial_conditions):
        self.run_conditions[run] = initial_conditions
        if self.system_size > 1:
            self.max_abs_values[run] = np.max(np.abs(timeseries[:, self.system_size:]), axis=1)
        else:
            self.max_abs_values[run] = np.squeeze(np.abs(timeseries[:, self.system_size:]))
        self.fin_abs_values[run] = np.abs(timeseries[-1, :])

        timeseries[:, :self.system_size] = np.mod(timeseries[:, :self.system_size] + np.pi, 2 * np.pi) - np.pi

        self.converged[run] = 1 * self.test_convergence(self.max_abs_values[run], self.times)

        for t in timeseries:
            self.tree.insert(run, (t[0],t[1],t[0],t[1]), obj=self.converged[run])

def precompute_basin(clean=False):

    np.random.seed(0)

    # Functions decorated with run_on_master will only be run on the master node, you need to broadcast the result
    # to the other nodes.
    @run_on_master
    def prep_dir(sim_dir):
        # delete old data
        if clean:
            os.remove(os.path.join(os.getcwd(), "simulation_data", "rtree.dat"))
            os.remove(os.path.join(os.getcwd(),"simulation_data",  "rtree.idx"))
        #
        p = Property(leaf_capacity=1000, fill_factor=0.9)
        tree = Rtree(os.path.join("simulation_data", "rtree"), interleaved=True, properties=p)
        #
        result_file = os.path.join("simulation_data", "precomputed_basin.hdf")
        batch_dir = os.path.join(sim_dir, "batch_data")
        if not os.path.exists(batch_dir):
            os.makedirs(batch_dir)
        return result_file, batch_dir, tree

    res = prep_dir("simulation_data")
    res = comm.bcast(res, root=0)
    result_file, batch_dir, tree = res

    times = np.append(0, np.logspace(-6, 2, 1000))

    brp = BatchRunParameters(number_of_batches=20, simulations_per_batch=20)
    pars = SwingPars(sytem_size=1)
    gen = SwingGen(pars)
    rcg = RCGenGrid(brp, pars, max_rand=[-np.pi, np.pi, -20., 20.])
    obs = BasinObs(brp, pars, tree, times=times, batch_dir=batch_dir)

    run_simulation(result_file, brp, gen, rcg, obs, times, mpi_enabled=True)

def ftbs_curves():


    # TODO: plot real lyapunov function and return-environment function
    # TODO: uncertainty for FTBS, for fixed T Bernoulli, for different T not independent ...

    # run a full simulation run of the Lorenz system, 10 batches, 10 simulations in each batch, default parameters.

    np.random.seed(0)

    # Functions decorated with run_on_master will only be run on the master node, you need to broadcast the result
    # to the other nodes.
    @run_on_master
    def prep_dir(sim_dir):
        result_file = os.path.join("simulation_data", "results_ftbs.hdf")
        batch_dir = os.path.join(sim_dir, "batch_data")
        if not os.path.exists(batch_dir):
            os.makedirs(batch_dir)
        return result_file, batch_dir

    res = prep_dir("simulation_data")
    res = comm.bcast(res, root=0)
    result_file, batch_dir = res

    # set size of S_\epsilon
    epsilon = np.logspace(-6, -1, 6)

    brp = BatchSwing(number_of_batches=200, simulations_per_batch=100, time=400, step=0.1, filt=epsilon)
    pars = SwingPars(sytem_size=1)
    gen = SwingGen(pars)
    rcg = SwingUniform(brp, pars, max_rand=[np.pi/3., 5.])
    # rcg = libftbs.SwingGauss(brp, pars, max_rand=[np.pi, 2.])

    Crho =  1. / rcg.max_rand[0] + 1. / rcg.max_rand[1]

    obs = SwingObs(brp, pars, batch_dir, Crho)

    run_simulation(result_file, brp, gen, rcg, obs, brp.times, mpi_enabled=True)

def find_idx(vals, e, first=False, less=False, nothing=[]):
    if less:
        idx = np.where(np.array(vals) < e)[0]
    else:
        idx = np.where(np.array(vals) > e)[0]
    # print idx
    if len(idx) is 0:
        return nothing
    else:
        if first:
            return idx[0]
        else:
            return idx[-1]

def test_convergence(fmax, times, abs_tol=1., inc_crit=0., ts_frac=0.5,  talk=0):
    """

    :param fmax:
    :param times:
    :param abs_tol:
    :param inc_crit:
    :param ts_frac:
    :param talk:
    :return:
    """
    from scipy.stats import linregress

    L = len(times)

    slope, intercept, r_value, p_value, std_err = linregress(times[int(ts_frac * L):], np.log(fmax)[int(ts_frac * L):])
    if talk:
        print r"slope {:3f}, intercept {:3f}, r_val {:3f}, p_val {:3f},std_err {:3f}".format(slope, intercept, r_value,
                                                                                             p_value, std_err)
    if slope <= inc_crit and fmax[-1] < abs_tol:
        return True
    else:
        return False

@run_on_master
def load_from_json(filename, shuffle_power=False, seed=None, coupling_factor=1., alpha=0.):
    import json
    from scipy.sparse import lil_matrix, csr_matrix

    with open(filename) as f:
        data = json.load(f)

    NN = len(data["nodes"])

    P = np.empty(NN)
    damping = np.empty(NN)

    for n in data["nodes"]:
        P[n["id"]] = n["input_power"]
        damping[n["id"]] = n["damping"]

    if shuffle_power:
        if seed is not None:
            np.random.set_state(seed)
        np.random.shuffle(P)

    Y = lil_matrix((NN,NN), dtype=np.complex128)

    # The json file needs to contain "links" with "source" and "target" and a
    # "coupling" field.

    for e in data["links"]:
        Y[e["source"], e["target"]] = e["coupling"] * 1.j
        Y[e["target"], e["source"]] = e["coupling"] * 1.j

    diagonal_sum = Y.dot(np.ones(NN))

    for i in range(NN):
        Y[i,i] = - diagonal_sum[i]

    spars = SwingPars(NN)

    spars.k = csr_matrix(Y) * np.exp(1.j * alpha)
    spars.coupling_factor = coupling_factor
    spars.damping_coupling = damping
    spars.input_power = P

    spars.fix_point = get_fixpoint(spars)

    return spars

@run_on_master
def get_fixpoint(swingpar, guess=None):
    assert isinstance(swingpar, SwingPars)
    # def rootfunc(y):
    #     w_global = y[0]
    #     phase = np.exp(1.j * y)
    #     phase[0] = 1.
    #     flow = np.conj(swingpar.k.dot(phase))
    #     return swingpar.input_power - swingpar.damping_coupling * w_global \
    #            - np.real(phase * swingpar.k.dot(phase).conjugate())
    def rootfunc(y):
        # shift phases such that fp is at origin
        phases = np.exp(1.j * (y[:swingpar.system_size]))
        # watch!! I needed to change imag to real part of power flow (see statistic rhs below)
        return np.append(y[swingpar.system_size:], swingpar.input_power - swingpar.damping_coupling * y[swingpar.system_size:] - np.real(phases * swingpar.k.dot(
            phases).conjugate()))

    g2 = np.zeros(swingpar.system_dimension)

    if guess is not None:
        g2[1:] = guess[1:swingpar.system_size] - guess[0]
        g2[0] = np.average(guess[swingpar.system_size:])

    from scipy.optimize import root
    from scipy.integrate import odeint

    fixpoint = np.zeros(swingpar.system_dimension)

    res = root(rootfunc, g2, method='Krylov', tol=1e-07)


    if res.success:
        #fixpoint[1:swingpar.system_size] = res.x[1:]
        #fixpoint[swingpar.system_size:] = res.x[0]
        ts = odeint(lambda y, t: rootfunc(y), res.x, np.linspace(0, 200, 1000))
        fixpoint = ts[-1, :]
        return fixpoint
    else:
        return None

def network_ftbs_curves():


    np.random.seed(0)
    # get the initial state of the RNG
    # https://stackoverflow.com/questions/32172054/how-can-i-retrieve-the-current-seed-of-numpys-random-number-generator
    st0 = np.random.get_state()
    np.random.set_state(st0)


    # Functions decorated with run_on_master will only be run on the master node, you need to broadcast the result
    # to the other nodes.
    @run_on_master
    def prep_dir(sim_dir):
        #result_file = os.path.join("simulation_data", "results_ftbs_network_largepert.hdf")
        result_file = os.path.join("simulation_data", "results_ftbs_network.hdf")
        batch_dir = os.path.join(sim_dir, "batch_data")
        if not os.path.exists(batch_dir):
            os.makedirs(batch_dir)
        return result_file, batch_dir

    res = prep_dir("simulation_data")
    res = comm.bcast(res, root=0)
    result_file, batch_dir = res

    res2 = load_from_json("northern.json", shuffle_power=False, seed=st0, coupling_factor=1., alpha=0.)
    res2 = comm.bcast(res2, root=0)
    swingpar = res2

    # set size of S_\epsilon
    epsilon = np.array([1e-6, 1e-4, 1e-3, 1e-1, 1, 2])
    freq_var = .1
    phase_var = .1 # np.pi / 3.

    gen = SwingGen(swingpar)
    brp = BatchSwing(number_of_batches=200, simulations_per_batch=100, time=800, step=0.1, filt=epsilon)
    rcg = SwingUniformGlobal(brp, swingpar, max_rand=[phase_var, freq_var])
    obs = SwingObsNetwork(brp, swingpar, batch_dir, freq_var=freq_var, phase_var=phase_var)

    run_simulation(result_file, brp, gen, rcg, obs, brp.times, mpi_enabled=True)


if __name__ == "__main__":
    #precompute_basin()
    network_ftbs_curves()


from pik_parallel_tools.batch_framework import *

import numpy as np

import scipy.spatial.distance as sp
from rtree.index import Rtree, Property # install via conda install --channel https://conda.anaconda.org/IOOS rtree

from matplotlib import colors, cycler, pyplot
from matplotlib.colors import LinearSegmentedColormap, hex2color
import matplotlib.cm as cmx

import awesomeplot.core as ac

apply_filter = np.vectorize(lambda x, f: 1. if x > f else 0.)

nan_filter = np.vectorize(lambda x: 1. if np.isfinite(x) else 0.)

regularized_norm = np.vectorize(lambda x: 1./x if not x == 0. else 1.)

regularized_inv = np.vectorize(lambda x: 1./x if not x == 0. else 0.)


def lyapunov_func():

    import libftbs

    np.random.seed(0)

    brp = libftbs.BatchSwing(time=100, step=0.001)
    pars = libftbs.SwingPars()
    gen = libftbs.SwingGen(pars)
    rcg = libftbs.SwingUniform(brp, pars, max_rand=[np.pi, 10.])
    obs = libftbs.SwingObs(brp, pars, ".", 0)

    brp.times = np.arange(0, 100, brp.step)

    timeseries = gen.create_ts(rcg.gen(0, 0), brp.times)

    canvas = ac.Plot.paper(font_scale=1.5)

    timeseries[:, 0] = np.mod(timeseries[:, 0] - np.pi, 2. * np.pi) - np.pi

    fig = canvas.add_lineplot(brp.times, {r"$\phi$": timeseries[:, 0], r"$\omega$": timeseries[:, 1]})
    canvas.set_log(fig, log="x")
    fig.suptitle('timeseries')


    n = 100
    x, y = np.meshgrid(
        np.linspace(-np.pi, np.pi, n),
        np.linspace(-15., 15., n)
    )

    l = np.zeros_like(x)
    for i in xrange(n):
        for j in xrange(n):
            l[i, j] = obs.lyapunov_prod(np.array([x[i, j], y[i, j]]), pars.fix_point)

    fig = canvas.add_contour(x[0, :], y[:, 0], l, labels=[r'$\phi$', r'$\omega$', r'$x^tLx$'], pi="x")
    fig.axes[0].plot(timeseries[:, 0], timeseries[:, 1], 'k')
    fig.suptitle('linear Lyapunov function')

    n = 100
    x, y = np.meshgrid(
        np.linspace(-np.pi, np.pi, n),
        np.linspace(-15., 15., n)
    )

    lf = pars.k * (1. - np.cos(x)) + y * y / 2. - pars.p * x + pars.p * pars.fix_point[0] - pars.k * (1. - np.cos(pars.fix_point[0]))
    fig = canvas.add_contour(x[0, :], y[:, 0], lf / np.abs(lf).max(), labels=[r'$\phi$', r'$\omega$', r'$V$'], nlevel=20,  pi="x", sym=True, fixed_scale=(-1, 1))
    fig.axes[0].plot(timeseries[:, 0], timeseries[:, 1], 'k')
    fig.suptitle('Lyapunov function')

    canvas.show()

def trajectory():
    from scipy.interpolate import griddata
    from libescape import BatchSwing, SwingPars, SwingGen, SwingStochastic

    np.random.seed(0)

    grid_x, grid_y = np.mgrid[-np.pi:np.pi:1000j, -20.:20.:1000j]

    tree = Rtree(os.path.join("simulation_data", "rtree"), interleaved=True)

    bbox = [-np.pi, -20., np.pi, 20.]
    coords = np.array([n.bounds[::2] for n in tree.intersection(bbox, objects=True)])
    basin = np.array([n.object for n in tree.intersection(bbox, objects=True)])

    grid_z2 = griddata(coords, basin, (grid_x, grid_y), method='nearest')

    brp = BatchSwing(number_of_batches=10, simulations_per_batch=1, step=0.1, filt=1e-6, Tmin=1, Tmax=100, nT=100)
    par = SwingPars()
    rcg = SwingStochastic(brp, par, max_rand=[np.pi / 3., 5.], rho="uniform")
    gen = SwingGen(par, brp, rcg.single, tree)

    canvas = ac.Plot.paper(font_scale=2)

    for batch in range(len(brp.deltaT)):


        fig = canvas.add_contour(grid_x[:, 0], grid_y[0, :], grid_z2.T, labels=[r"$\phi$", r"$\omega$", ""], pi="x", sym=False, fixed_scale=(0, 1))
        ts = gen.create_ts([batch, None], None)
        fig.axes[0].plot(ts[:, 0], ts[:, 1], "k-", alpha=0.3)

        canvas.save("temp/traj_basin_T{}".format(brp.deltaT[batch]), fig)

    pass





def plot_basin(file="rtree"):
    from scipy.interpolate import griddata
    grid_x, grid_y = np.mgrid[-np.pi:np.pi:1000j, -20.:20.:1000j]

    tree = Rtree(os.path.join("simulation_data", file), interleaved=True)

    bbox = [-np.pi, -20., np.pi, 20.]
    coords = np.array([n.bounds[::2] for n in tree.intersection(bbox, objects=True)])
    basin = np.array([n.object for n in tree.intersection(bbox, objects=True)])

    grid_z2 = griddata(coords, basin, (grid_x, grid_y), method='nearest')

    canvas = ac.Plot.paper(font_scale=2)
    fig = canvas.add_contour(grid_x[:, 0], grid_y[0, :], grid_z2.T, labels=[r"$\phi$", r"$\omega$", ""], pi="x", sym=False, fixed_scale=(0, 1))
    canvas.show()
    pass



@run_on_master
def plot_ftbs_curve(result_file="simulation_data/results_ftbs.hdf"):
    from libftbs import BatchSwing

    stepfunc = np.vectorize(lambda x, f: 0. if x < f else 1.)

    print "read brp"

    with h5py.File(result_file, mode='r') as h5f:
        brp = hdfutils.init_class_from_hdf5_group(h5f["batch_run_parameters"], BatchSwing, BatchSwing())

    times = brp.times
    samplesize = brp.simulations_per_batch * brp.number_of_batches

    returns = np.zeros([samplesize, len(brp.filt)])
    fin_abs = np.zeros([samplesize, 2])

    print "read data", brp.number_of_batches, brp.simulations_per_batch

    with h5py.File(result_file, mode='r') as h5f:
        for i in xrange(brp.number_of_batches):
            returns[i * brp.simulations_per_batch: (i + 1) * brp.simulations_per_batch] = np.array(h5f["batch" + str(i)]["return_time"])
            fin_abs[i * brp.simulations_per_batch: (i + 1) * brp.simulations_per_batch] = np.array(h5f["batch" + str(i)]["fin_abs_values"])

    returns = np.sort(returns, axis=0)

    print "calc ftbs", len(brp.filt), len(times)

    bs = 1. * sum(1. - stepfunc(fin_abs[:, 1], 1)) / samplesize
    ftbs = np.zeros([len(times), len(brp.filt)])

    print bs, np.sqrt(bs * (1. - bs) / samplesize), samplesize

    for i, f in enumerate(brp.filt): # epsilon
        for j, t in enumerate(times): # delta T
            returned = (1 - stepfunc(returns[:, i], t))  # * (1 - stepfunc(obs.fin_abs_values[:, 1], 1e-3))
            ftbs[j, i] = 1. * sum(returned) / samplesize

    print "plot ftbs curve"

    lines = {r"$\epsilon$={:.0E}".format(brp.filt[i]): ftbs[:, i] for i in range(len(brp.filt))}
    lines[r"$\beta$"] = bs * np.ones_like(times)




    canvas = ac.Plot(output="paper", font_scale=1.5)
    canvas.set_default_colours("discrete")

    def sorting(x):
        try:
            out = float(x.split("-")[-1])
        except:
            out = 0
        return out

    fig = canvas.add_lineplot(times, lines,  marker="",
                                         labels=["T", r"$\beta(T)$"],
                                         sortfunc=sorting)
    fig.axes[0].set_ylim([0, 1.1])

    print "plot T_ind"

    lines = {}
    thresholds = np.logspace(-8, -1, 4)
    for eps2 in thresholds:
        tc = np.zeros_like(brp.filt)
        for i, eps in enumerate(brp.filt):
            try:
                tc[i] = times[np.where(np.abs(bs - ftbs[:, i]) < eps2)[0][0]]
            except:
                tc[i] = np.nan
                #print eps, eps2, np.min(np.abs(bs - ftbs[:, i])), np.abs(bs - ftbs[:, i])
        lines[r"$\delta$={:.0E}".format(eps2)] = tc

    print lines

    np.save("Tind_data_delta{:.0E}".format(thresholds[-1]), np.c_[brp.filt, lines[r"$\delta$={:.0E}".format(thresholds[-1])]])

    fig2 = canvas.add_lineplot(x=brp.filt,
                               lines=lines,
                               labels=[r"$\epsilon$", r"$T_{ind}(\epsilon, \delta)$"],
                               sortfunc=lambda x: float(x.split("-")[-1])
                               )
    #canvas.set_log(fig2, "x")

    print lines

    fig2.axes[0].plot(brp.filt, 450 * np.log(brp.filt ** (-0.05)), "k-")

    fig2.axes[0].set_xscale("log")

    canvas.show()

    canvas.save(["../figures/swing_ftbs", "../figures/swing_Tc"])




def plot_escape_rate(result_file="simulation_data/results_escape_rate.hdf"):
    from libescape import BatchSwing

    with h5py.File(result_file, mode='r') as h5f:
        brp = hdfutils.init_class_from_hdf5_group(h5f["batch_run_parameters"], BatchSwing)

    escape_time = np.zeros([brp.number_of_batches, brp.simulations_per_batch])
    print brp.number_of_batches, brp.simulations_per_batch
    print brp.deltaT

    with h5py.File(result_file, mode='r') as h5f:
        for i in xrange(brp.number_of_batches):
            escape_time[i, :] = np.array(h5f["batch" + str(i)]["escape_time"]) #- brp.deltaT[i]

    canvas = ac.Plot.paper(font_scale=2)

    ## escape time distribution
    fig = canvas.add_distplot(x=brp.deltaT, y=escape_time, text=False, labels=["T", r"$T_e$"], marker="")
    canvas.set_log(fig, log="y")

    ## from ftbs above
    bs = 0.9874
    samplesize = 20000  # batches * simulations
    bs_err = np.sqrt(bs * (1 - bs) / samplesize)

    ## choose epsilon and delta

    eps = 1e-8

    ## calculate number of jumps
    nt = np.zeros_like(escape_time)
    for i, val in enumerate(brp.deltaT):
        nt[i] = escape_time[i] / val

    max_time = escape_time.max()
    times = np.logspace(-9, np.log10(max_time), 500)

    ## remain probability
    p = np.zeros([len(brp.deltaT), len(times)])
    p_asymp = np.zeros_like(brp.deltaT, dtype=np.float)
    p_asymp_err = np.zeros_like(p_asymp)

    for idx, deltaT in enumerate(brp.deltaT):
        # filter returns 1 if escape_time > t, i.e. trajectory remains at least until time t
        temp = [np.sum(apply_filter(escape_time[idx, :], t)) for t in times]
        # divide by brp.simulations_per_batch
        p[idx, :] = temp * regularized_norm(escape_time.shape[1])
        # calculate n(t) for each interval T
        jumps = np.array(times / deltaT, dtype=np.float)
        # invert formula
        p[idx, :] = np.exp(1. * np.log(p[idx, :]) / jumps)
        # average over the last bit
        p_asymp[idx] = np.mean(np.trim_zeros(p[idx, :], "b")[-20:])
        p_asymp_err[idx] = np.std(np.trim_zeros(p[idx, :], "b")[-20:])


    ## plot estimate using inverted formula

    # hack for continous line colours
    lin_colours_3 = LinearSegmentedColormap.from_list(
        'linear', [(0., 'grey'), (0.5, hex2color('#ffd24d')), (1, canvas.pik_colours.colors[0])]
    )
    lin_colours_3.set_bad(hex2color('#8e908f'))

    cNorm = colors.Normalize(vmin=brp.deltaT[0], vmax=brp.deltaT[-1])
    scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=lin_colours_3)
    c = [scalarMap.to_rgba(v) for v in brp.deltaT[::-1]]

    canvas.update_params({
                'axes.prop_cycle': cycler('color', c),
                'image.cmap': lin_colours_3.name
            })

    def rolling_window(a, window):
        shape = a.shape[:-1] + (a.shape[-1] - window + 1, window)
        strides = a.strides + (a.strides[-1],)
        return np.lib.stride_tricks.as_strided(a, shape=shape, strides=strides)

    p[p == 0] = np.nan

    delta = 1e-6

    selection = [0, 44, 57, 70, -1]

    lines = {r"$\Delta T=${}".format(t): p[i, :] for i, t in enumerate(brp.deltaT[selection])}

    fig = canvas.add_lineplot(x=times,
                              lines=lines,
                              # shades={r"$\beta^{n(t)}$": [psurv_m - psurv_s, psurv_m + psurv_s]},
                              labels=[r"time $t$", r"$\hat{p}$"],
                              sortfunc=lambda x: -int(x.split("=$")[-1]),
                              legend=False,
                              marker=""
                              )
    fig.set_size_inches(6, 5)
    fig.axes[0].set_ylim([0.88, 1.001])
    fig.axes[0].set_xlim([10, times.max()])

    # for curve in brp.deltaT[selection]:
    #     cc = lines[r"$\Delta T=${}".format(curve)]
    #     fig.axes[0].text(times[len(cc)], cc[-1], r"$\Delta T=${}".format(curve))


    canvas.set_log(fig, log="x")


    ind_time = sorted(np.where(np.nanmin(p, axis=1) > (bs - eps - delta))[0])[0]
    print brp.deltaT[ind_time]

    fig.axes[0].fill_between(times, (bs - bs_err - eps - delta) * np.ones_like(times),
                     (bs + bs_err - eps - delta) * np.ones_like(times),
                     edgecolor=canvas.pik_colours.colors[-1], alpha=0.3,
                     facecolor=canvas.pik_colours.colors[-1], zorder=10)
    #fig.axes[0].plot(times, p[0, :], canvas.pik_colours.colors[-1])
    fig.axes[0].plot(times, p[ind_time, :], canvas.pik_colours.colors[1])
    #fig.axes[0].plot(times, p[-1, :], canvas.pik_colours.colors[-1])


    data = np.load("Tind_data_delta{:.0E}.npy".format(1e-08))
    print data

    fig = canvas.add_lineplot(brp.deltaT,
                              lines={"p_asymp": p_asymp},
                              shades={"p_asymp": [p_asymp - p_asymp_err, p_asymp + p_asymp_err]},
                              labels=[r"T", r"$\lim_{t\rightarrow \infty}\hat p$"],
                              marker="",
                              legend=False
                              )
    fig.axes[0].plot(brp.deltaT, bs * np.ones_like(brp.deltaT), "k:")

    fig.axes[0].plot(data[:, 1], bs - data[:, 0] - delta, "k+") # beta -eps - delta

    canvas.show()
    canvas.save(["../figures/escape_time", "../figures/swing_psurv", "../figures/swing_psurv_asymp"])



if __name__ == "__main__":
    trajectory()
    #plot_basin()
    #plot_ftbs_curve()
    #plot_escape_rate()
